#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Should be uncommented after fixing vndk-sp violation is fixed.
PRODUCT_FULL_TREBLE_OVERRIDE := true

ifneq ($(filter full_erdv9630_qr, $(TARGET_PRODUCT)),)
CONF_NAME = conf_qr
else
ifneq ($(filter full_erdv9630_qrs, $(TARGET_PRODUCT)),)
CONF_NAME = conf_qrs
else
CONF_NAME = conf_pqr
TARGET_RECOVERY_FSTAB := device/samsung/erdv9630/conf_pqr/fstab.exynos9630
endif
endif

TARGET_LINUX_KERNEL_VERSION := 4.14
TARGET_BOARD_INFO_FILE := device/samsung/erdv9630/board-info.txt

# HACK : To fix up after bring up multimedia devices.

TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-a
TARGET_CPU_ABI := arm64-v8a
TARGET_CPU_VARIANT := generic

TARGET_2ND_ARCH := arm
TARGET_2ND_ARCH_VARIANT := armv7-a-neon
TARGET_2ND_CPU_ABI := armeabi-v7a
TARGET_2ND_CPU_ABI2 := armeabi
TARGET_2ND_CPU_VARIANT := cortex-a15
TARGET_CPU_SMP := true

TARGET_NO_RADIOIMAGE := true
TARGET_NO_BOOTLOADER := true

TARGET_DEVICE_NAME := erdv9630
TARGET_SOC_NAME := exynos

# For Enable A/B device
ifeq ($(AB_ENABLE),true)
AB_OTA_UPDATER := true
else
AB_OTA_UPDATER := false
endif

# bionic libc options
#ARCH_ARM_USE_MEMCPY_ALIGNMENT := true
#BOARD_MEMCPY_ALIGNMENT := 64
#BOARD_MEMCPY_ALIGN_BOUND := 32768
BOARD_MEMCPY_AARCH32 := true
BOARD_MEMCPY_MNGS := true

# SMDK common modules
BOARD_SMDK_COMMON_MODULES := liblight

OVERRIDE_RS_DRIVER := libRSDriverArm.so
BOARD_EGL_CFG := device/samsung/erdv9630/$(CONF_NAME)/egl.cfg
#BOARD_USES_HGL := true
USE_OPENGL_RENDERER := true
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3
BOARD_USES_EXYNOS5_COMMON_GRALLOC := true
BOARD_USES_EXYNOS_GRALLOC_VERSION := 3
BOARD_USES_ALIGN_RESTRICTION := true

# Graphics
BOARD_USES_EXYNOS_DATASPACE_FEATURE := true

# Storage options
BOARD_USES_SDMMC_BOOT := false
BOARD_USES_SDMMC_BOOT := false
BOARD_USES_UFS_BOOT := true
BOARD_KERNEL_CMDLINE := androidboot.selinux=enforce
BOARD_USES_VENDORIMAGE := true
TARGET_COPY_OUT_VENDOR := vendor
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_VENDORIMAGE_PARTITION_SIZE := 314572800

TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_USE_F2FS := true
BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE := f2fs
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2867200000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 11796480000
ifeq ($(AB_OTA_UPDATER), false)
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_CACHEIMAGE_PARTITION_SIZE := 69206016
endif
BOARD_FLASH_BLOCK_SIZE := 4096
BOARD_MOUNT_SDCARD_RW := true

########################
# Video Codec
########################
# 0. Default C2
BOARD_USE_DEFAULT_SERVICE := true

# 2. Exynos OMX
BOARD_USE_DMA_BUF := true
BOARD_USE_NON_CACHED_GRAPHICBUFFER := true
BOARD_USE_GSC_RGB_ENCODER := true
BOARD_USE_CSC_HW := false
BOARD_USE_S3D_SUPPORT := false
BOARD_USE_DEINTERLACING_SUPPORT := true
BOARD_USE_HEVCENC_SUPPORT := true
BOARD_USE_HEVC_HWIP := false
BOARD_USE_VP9DEC_SUPPORT := true
BOARD_USE_VP9ENC_SUPPORT := true
BOARD_USE_WFDENC_SUPPORT := false
BOARD_USE_CUSTOM_COMPONENT_SUPPORT := false
BOARD_USE_VIDEO_EXT_FOR_WFD_HDCP := true
BOARD_USE_SINGLE_PLANE_IN_DRM := true
BOARD_USE_WA_ION_BUF_REF := true
BOARD_USE_FRAMERATE_THRESH_HOLD := 5
########################

#
# AUDIO & VOICE
#
BOARD_USES_GENERIC_AUDIO := true

# Primary AudioHAL ConfigurIation
#BOARD_USE_COMMON_AUDIOHAL := true
#BOARD_USE_CALLIOPE_AUDIOHAL := false
#BOARD_USE_AUDIOHAL := false
#BOARD_USE_AUDIOHAL_COMV1 := true

#  Audio Feature Configuration
#BOARD_USE_OFFLOAD_AUDIO := true
#BOARD_USE_OFFLOAD_EFFECT := true
#BOARD_USE_USB_OFFLOAD := true
#BOARD_USE_AUDIO_MONITOR := true

# SoundTriggerHAL Configuration
#BOARD_USE_SOUNDTRIGGER_HAL := true
#BOARD_USE_SOUNDTRIGGER_HAL_MMAP := false

# Audio Dump Tool
#BOARD_USES_AUDIO_LOGGING_SERVICE := true

# CAMERA
BOARD_BACK_CAMERA_ROTATION := 90
BOARD_FRONT_CAMERA_ROTATION := 270
BOARD_SECURE_CAMERA_ROTATION := 0
BOARD_BACK_CAMERA_SENSOR := SENSOR_NAME_S5KGM1SP
BOARD_FRONT_CAMERA_SENSOR := SENSOR_NAME_S5K4H5YC
BOARD_SECURE_CAMERA_SENSOR := SENSOR_NAME_NOTHING

BOARD_BACK_1_CAMERA_SENSOR := SENSOR_NAME_S5K5E9
BOARD_BACK_1_CAMERA_SENSOR_OPEN := false
BOARD_FRONT_1_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_2_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_2_CAMERA_SENSOR_OPEN := false
BOARD_FRONT_2_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_FRONT_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING

BOARD_CAMERA_USES_DUAL_CAMERA := true
BOARD_DUAL_CAMERA_REAR_ZOOM_MASTER := CAMERA_ID_BACK_0
BOARD_DUAL_CAMERA_REAR_ZOOM_SLAVE := CAMERA_ID_BACK_1
BOARD_DUAL_CAMERA_REAR_PORTRAIT_MASTER := CAMERA_ID_BACK_1
BOARD_DUAL_CAMERA_REAR_PORTRAIT_SLAVE := CAMERA_ID_BACK_0
BOARD_DUAL_CAMERA_FRONT_PORTRAIT_MASTER := CAMERA_ID_FRONT_0
BOARD_DUAL_CAMERA_FRONT_PORTRAIT_SLAVE := CAMERA_ID_FRONT_1

BOARD_CAMERA_USES_COMBINE_PLUGIN := true
BOARD_CAMERA_USES_DUAL_CAMERA_SOLUTION_FAKE := true
BOARD_CAMERA_USES_DUAL_CAMERA_SOLUTION_ARCSOFT := false
BOARD_SUPPORT_FACTORY_CHECK_ACTIVE_CAMERA := true

#BOARD_CAMERA_USES_LLS_SOLUTION := true
BOARD_CAMERA_USES_CAMERA_SOLUTION_VDIS := true
BOARD_CAMERA_USES_SLSI_PLUGIN := true
BOARD_CAMERA_USES_PIPE_HANDLER := true
#BOARD_CAMERA_USES_HIFI_LLS_CAPTURE := true
#BOARD_CAMERA_USES_SCENE_DETECT := true
BOARD_CAMERA_USES_HIFI_CAPTURE := true
BOARD_CAMERA_USES_EXYNOS_VPL := true
#BOARD_CAMERA_USES_NIGHT_SHOT_YUV := true
BOARD_CAMERA_USES_SLSI_VENDOR_TAGS := true
#BOARD_CAMERA_USES_REMOSAIC_SENSOR := false
#BOARD_CAMERA_USES_SENSOR_LISTENER := true
#BOARD_CAMERA_USES_SENSOR_GYRO_FACTORY_MODE := true
BOARD_CAMERA_USES_P3_IN_EXIF := true
BOARD_CAMERA_USES_SBWC := true
BOARD_CAMERA_USES_3AA_DNG := true
BOARD_CAMERA_USES_CLAHE := true

# HWComposer
BOARD_HWC_VERSION := libhwc2.1
TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := false
BOARD_HDMI_INCAPABLE := true
TARGET_USES_HWC2 := true
HWC_SKIP_VALIDATE := true
HWC_SUPPORT_COLOR_TRANSFORM := true
#TARGET_HAS_WIDE_COLOR_DISPLAY := true
TARGET_USES_DISPLAY_RENDER_INTENTS := true
#BOARD_USES_DISPLAYPORT := true
#BOARD_USES_EXTERNAL_DISPLAY_POWERMODE := true
BOARD_USES_EXYNOS_AFBC_FEATURE := true
#BOARD_USES_HDRUI_GLES_CONVERSION := true
BOARD_USES_HWC_SERVICES := true
#BOARD_USES_SF_PERF_MODE := true
BOARD_USES_HDR_INTERFACE := true
BOARD_USES_DQE_INTERFACE := true
VSYNC_EVENT_PHASE_OFFSET_NS := 0
SF_VSYNC_EVENT_PHASE_OFFSET_NS := 0

BOARD_LIBHDR_PLUGIN := libhdr_plugin_exynos9630
BOARD_LIBHDR10P_META_PLUGIN := libhdr10p_meta_plugin_default

# WifiDisplay
# BOARD_USES_WIFI_DISPLAY:= true

# SCALER
#BOARD_USES_DEFAULT_CSC_HW_SCALER := true
#BOARD_DEFAULT_CSC_HW_SCALER := 4
#BOARD_USES_SCALER_M2M1SHOT := true

# LIBHWJPEG
TARGET_USES_UNIVERSAL_LIBHWJPEG := true

# IMS
#BOARD_USES_IMS := true

# Board hardware name
BOARD_KERNEL_CMDLINE += androidboot.hardware=$(TARGET_SOC)

# Device Tree
BOARD_USES_DT := true

# PLATFORM LOG
TARGET_USES_LOGD := true

# ART
#ADDITIONAL_BUILD_PROPERTIES += dalvik.vm.image-dex2oat-filter=speed
#ADDITIONAL_BUILD_PROPERTIES += dalvik.vm.dex2oat-filter=speed

# FMP
BOARD_USES_FMP_DM_CRYPT := true
BOARD_USES_FMP_FSCRYPTO := true
BOARD_USES_METADATA_PARTITION := true

# SELinux Platform Private policy for exynos
BOARD_PLAT_PRIVATE_SEPOLICY_DIR := device/samsung/sepolicy/private

# SELinux Platform Public policy for exynos
BOARD_PLAT_PUBLIC_SEPOLICY_DIR := device/samsung/sepolicy/public

# SELinux policies
SAMSUNG_SEPOLICY := device/samsung/sepolicy/common \
				    device/samsung/sepolicy/exynos9630 \
				    device/samsung/erdv9630/sepolicy

ifeq (, $(findstring $(SAMSUNG_SEPOLICY), $(BOARD_SEPOLICY_DIRS)))
BOARD_SEPOLICY_DIRS += $(SAMSUNG_SEPOLICY)
endif

# SECCOMP policy
BOARD_SECCOMP_POLICY = device/samsung/erdv9630/seccomp_policy

# NFC
include device/samsung/erdv9630/nfc/BoardConfig-nfc.mk

#include vendor/samsung_slsi/sscr_tools/BoardConfig.mk

#CURL
#BOARD_USES_CURL := true

# VISION
# Exynos vision framework (EVF)
#TARGET_USES_EVF := true
# HW acceleration
#TARGET_USES_VPU_KERNEL := true
#TARGET_USES_SCORE_KERNEL := true
#TARGET_USES_CL_KERNEL := false

# radio/telephony
SUPPORT_DS := true
SUPPORT_NR := true

#SENSOR HUB
#BOARD_USES_EXYNOS_SENSORS_DUMMY := true
#BOARD_USES_EXYNOS_SENSORS_HAL := true

# GNSS
BOARD_USES_EXYNOS_GNSS_HAL := true
ifeq ($(BOARD_USES_EXYNOS_GNSS_HAL),)
BOARD_USES_EXYNOS_GNSS_DUMMY := true
endif

TARGET_BOARD_KERNEL_HEADERS := hardware/samsung_slsi/exynos/kernel-4.9-headers/kernel-headers

# SYSTEM SDK
ifneq ($(filter full_erdv9630_qr full_erdv9630_qrs, $(TARGET_PRODUCT)),)
BOARD_SYSTEMSDK_VERSIONS := 29
else
BOARD_SYSTEMSDK_VERSIONS := 28
endif

TARGET_USES_MKE2FS := true

#VNDK
BOARD_PROPERTY_OVERRIDES_SPLIT_ENABLED := true
BOARD_VNDK_VERSION := current

SOONG_CONFIG_NAMESPACES += libacryl
SOONG_CONFIG_libacryl += default_compositor \
                         default_scaler \
                         default_blter
SOONG_CONFIG_libacryl_default_compositor := fimg2d_L8FSBWCL
SOONG_CONFIG_libacryl_default_scaler := mscl_sbwcl
SOONG_CONFIG_libacryl_default_blter := fimg2d_9810_blter
BOARD_USE_GIANT_MSCL := true

# CBD (CP booting daemon)
SOONG_CONFIG_NAMESPACES += cbd
SOONG_CONFIG_cbd := version protocol flags
SOONG_CONFIG_cbd_version := v2
SOONG_CONFIG_cbd_protocol := sit
SOONG_CONFIG_cbd_flags := CONFIG_DUMP_LIMIT

#Keymaster
BOARD_USES_KEYMASTER_VER4 := true

# WiFi related defines
#BOARD_WPA_SUPPLICANT_DRIVER      := NL80211
#BOARD_HOSTAPD_DRIVER             := NL80211
#BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_slsi
#BOARD_HOSTAPD_PRIVATE_LIB        := lib_driver_cmd_slsi
#BOARD_HAS_SAMSUNG_WLAN           := true
#WPA_SUPPLICANT_VERSION           := VER_0_8_X
#BOARD_WLAN_DEVICE                := slsi
#WIFI_DRIVER_MODULE_ARG           := ""
#WLAN_VENDOR                      := 8
#SCSC_WLAN_DEVICE_NAME            := neus_s620
#WIFI_HIDL_FEATURE_DUAL_INTERFACE := true

# Bluetooth related defines
BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_SLSI := true
SOONG_CONFIG_NAMESPACES += libbt-vendor
SOONG_CONFIG_libbt-vendor := slsi
SOONG_CONFIG_libbt-vendor_slsi := true
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := hardware/samsung_slsi/libbt/include
BLUEDROID_HCI_VENDOR_STATIC_LINKING := false

# Enable BT/WIFI related code changes in Android source files
#CONFIG_SAMSUNG_SCSC_WIFIBT       := true

# Options for CHUB Firmware build
NANOHUB_TARGET := 'erd9630\ all'
NANOHUB_PATH := device/google/contexthub

# NS-IOT TEST SCRIPT. This should be REMOVED in ship build for security reason
#BOARD_USES_NSIOT_TESTSCRIPT := true

# Enable system-as-root
#BOARD_BUILD_SYSTEM_ROOT_IMAGE := true

# CONFIG For A/B device
ifeq ($(AB_OTA_UPDATER), true)
TARGET_NO_RECOVERY := true
BOARD_USES_RECOVERY_AS_BOOT := true
BOARD_KERNEL_CMDLINE += androidboot.boot_devices=13600000.ufs
endif

ifneq ($(filter full_erdv9630_qr full_erdv9630_qrs, $(TARGET_PRODUCT)),)
BOARD_KERNEL_CMDLINE += root=/dev/ram0
else
BOARD_KERNEL_CMDLINE += skip_initramfs
endif

# H/W align restriction of MM IPs
BOARD_EXYNOS_S10B_FORMAT_ALIGN := 64

# Enable AVB2.0
BOARD_AVB_ENABLE := true
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x04000000
ifeq ($(AB_OTA_UPDATER), false)
ifneq ($(filter full_erdv9630_qr full_erdv9630_qrs, $(TARGET_PRODUCT)),)
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x02D00000
else
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x02D00000
endif
endif
BOARD_DTBOIMG_PARTITION_SIZE := 0x00100000
BOARD_AVB_ALGORITHM := SHA256_RSA4096
BOARD_AVB_KEY_PATH := device/samsung/erdv9630/avbkey_rsa4096.pem
BOARD_AVB_RECOVERY_KEY_PATH := device/samsung/erdv9630/avbkey_rsa4096.pem
BOARD_AVB_RECOVERY_ALGORITHM := SHA256_RSA4096
BOARD_AVB_RECOVERY_ROLLBACK_INDEX := 0
BOARD_AVB_RECOVERY_ROLLBACK_INDEX_LOCATION := 0

TARGET_FS_CONFIG_GEN := device/samsung/erdv9630/config.fs

BOOT_SECURITY_PATCH := 2019-10-01
VENDOR_SECURITY_PATCH := 2019-10-01

# RENDERENGINE
# Support SBWC formats 0: DISABLE, 1: ENABLE, 2: ENABLE + SUPPORT LOSSY FORMAT
RENDERENGINE_SUPPORTS_SBWC_FORMAT := 2

# Add config to allow use more buffer in Triple-buffering
CONFIG_USE_EXTRA_BUFFER := 1

# Gralloc4
SOONG_CONFIG_NAMESPACES += arm_gralloc
SOONG_CONFIG_arm_gralloc := \
	gralloc_arm_no_external_afbc \
	mali_gpu_support_afbc_basic \
	gralloc_init_afbc \
	gralloc_ion_sync_on_lock

ifeq ($(BOARD_USES_EXYNOS_AFBC_FEATURE),true)
SOONG_CONFIG_arm_gralloc_gralloc_arm_no_external_afbc := false
else
SOONG_CONFIG_arm_gralloc_gralloc_arm_no_external_afbc := true
endif

SOONG_CONFIG_arm_gralloc_mali_gpu_support_afbc_basic := true
SOONG_CONFIG_arm_gralloc_gralloc_init_afbc := true
SOONG_CONFIG_arm_gralloc_gralloc_ion_sync_on_lock := $(BOARD_USES_GRALLOC_ION_SYNC)

# libExynosGraphicbuffer
SOONG_CONFIG_NAMESPACES += exynosgraphicbuffer
SOONG_CONFIG_exynosgraphicbuffer := \
	gralloc_version

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),3)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := three
endif

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := four
endif

ifeq ($(MALI_BUILD),true)
# Allow deprecated BUILD_ module types required to build Mali UMD
BUILD_BROKEN_USES_BUILD_COPY_HEADERS := true
BUILD_BROKEN_USES_BUILD_HOST_EXECUTABLE := true
BUILD_BROKEN_USES_BUILD_HOST_SHARED_LIBRARY := true
BUILD_BROKEN_USES_BUILD_HOST_STATIC_LIBRARY := true
endif

# USB (USB gadgethal)
SOONG_CONFIG_NAMESPACES += usbgadgethal
SOONG_CONFIG_usbgadgethal:= exynos_product
SOONG_CONFIG_usbgadgethal_exynos_product := default
