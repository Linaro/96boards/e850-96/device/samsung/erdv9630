/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <dlfcn.h>
//#define LOG_NDEBUG 0

#define LOG_TAG "ERD9630_PowerHAL"
#include <log/log.h>

#include <hardware/hardware.h>
#include <hardware/power.h>

#define EPIC_LIB_PATH			"/vendor/lib64/libepic_helper.so"

#define EPIC_LITTLE_MINLOCK_ID		(4)
#define EPIC_BIG_MINLOCK_ID		(6)
#define EPIC_C2_DISABLE_ID		(13)
#define EPIC_DEVMODE_ID			(14)

#define EPIC_ID_COUNT_APPLAUNCH		(4)
#define EPIC_ID_COUNT_INTERACTION	(1)

#define EPIC_APPLAUNCH_DURATION		(1500 * 1000)
#define EPIC_INTERACTION_DURATION	 (200 * 1000)

/*for EPIC*/
typedef void (*init_t)();
typedef int (*alloc_request_t)(int);
typedef int (*alloc_multi_request_t)(const int[], int);
typedef bool (*acquire_option_t)(int, unsigned int, unsigned int);
typedef bool (*acquire_multi_option_t)(int, const unsigned int [], const unsigned int [], int len);

#define container_of(addr, struct_name, field_name) \
	((struct_name *)((char *)(addr) - offsetof(struct_name, field_name)))

struct erd9630_power_module
{
	struct power_module base;
	pthread_mutex_t lock;
	bool low_power_mode;

	/* EPIC Handle */
	void *epic_handle;
	long epic_req_applaunch;
	long epic_req_interaction;

	init_t pfn_init;
	alloc_request_t pfn_alloc_request;
	alloc_multi_request_t pfn_alloc_multi_request;
	acquire_option_t pfn_acquire_option;
	acquire_multi_option_t pfn_acquire_multi_option;

	static int id_list_applaunch[EPIC_ID_COUNT_APPLAUNCH];
	static unsigned int value_list_applaunch[EPIC_ID_COUNT_APPLAUNCH];
	static unsigned int time_list_applaunch[EPIC_ID_COUNT_APPLAUNCH];

	static int id_list_interaction;
	static unsigned int value_list_interaction;
	static unsigned int time_list_interaction;
};

int erd9630_power_module::id_list_applaunch[] = { EPIC_DEVMODE_ID, EPIC_BIG_MINLOCK_ID, EPIC_LITTLE_MINLOCK_ID, EPIC_C2_DISABLE_ID };
unsigned int erd9630_power_module::value_list_applaunch[] = { 4, 2210000, 1794000, 1 };
unsigned int erd9630_power_module::time_list_applaunch[] = { EPIC_APPLAUNCH_DURATION, EPIC_APPLAUNCH_DURATION, EPIC_APPLAUNCH_DURATION, EPIC_APPLAUNCH_DURATION };

int erd9630_power_module::id_list_interaction = EPIC_BIG_MINLOCK_ID;
unsigned int erd9630_power_module::value_list_interaction = 936000;
unsigned int erd9630_power_module::time_list_interaction = EPIC_INTERACTION_DURATION;

static void epic_init(struct erd9630_power_module *erd9630)
{
	erd9630->epic_handle = dlopen("/vendor/lib64/libepic_helper.so", RTLD_NOW);

	if (erd9630->epic_handle == nullptr) {
		ALOGE("Couldn't load EPIC!\n");
		return;
	}

	erd9630->pfn_init = (init_t)dlsym(erd9630->epic_handle, "epic_init");
	erd9630->pfn_alloc_request = (alloc_request_t)dlsym(erd9630->epic_handle, "epic_alloc_request");
	erd9630->pfn_alloc_multi_request = (alloc_multi_request_t)dlsym(erd9630->epic_handle, "epic_alloc_multi_request");
	erd9630->pfn_acquire_option = (acquire_option_t)dlsym(erd9630->epic_handle, "epic_acquire_option");
	erd9630->pfn_acquire_multi_option = (acquire_multi_option_t)dlsym(erd9630->epic_handle, "epic_acquire_multi_option");

	if (erd9630->pfn_alloc_request == nullptr)
		erd9630->epic_req_interaction = -1;
	else
		erd9630->epic_req_interaction = erd9630->pfn_alloc_request(erd9630->id_list_interaction);

	if (erd9630->pfn_alloc_multi_request == nullptr)
		erd9630->epic_req_applaunch = -1;
	else
		erd9630->epic_req_applaunch = erd9630->pfn_alloc_multi_request(erd9630->id_list_applaunch, EPIC_ID_COUNT_APPLAUNCH);

	if (erd9630->pfn_init != nullptr)
		erd9630->pfn_init();
}

static void erd9630_power_init(struct power_module __unused *module)
{
	struct erd9630_power_module *erd9630 =
		container_of(module, struct erd9630_power_module, base);

	erd9630->low_power_mode = false;

	epic_init(erd9630);
}

static void erd9630_hint_interaction(struct erd9630_power_module *erd9630)
{
	if (erd9630->pfn_acquire_option == nullptr ||
		erd9630->epic_req_interaction == -1) {
		ALOGE("Couldn't acquire for interaction!\n");
		return;
	}

	erd9630->pfn_acquire_option(erd9630->epic_req_interaction, erd9630->value_list_interaction, erd9630->time_list_interaction);
}

static void erd9630_hint_launch(struct erd9630_power_module *erd9630)
{
	if (erd9630->pfn_acquire_multi_option == nullptr ||
		erd9630->epic_req_applaunch == -1) {
		ALOGE("Couldn't acquire multiple for app launch!\n");
		return;
	}

	erd9630->pfn_acquire_multi_option(erd9630->epic_req_applaunch, erd9630->value_list_applaunch, erd9630->time_list_applaunch, EPIC_ID_COUNT_APPLAUNCH);
}

static void erd9630_power_hint(struct power_module *module, power_hint_t hint, void* __unused data)
{
	struct erd9630_power_module *erd9630 = (struct erd9630_power_module *) module;

	if (erd9630->low_power_mode && hint != POWER_HINT_LOW_POWER)
		return ;

	pthread_mutex_lock(&erd9630->lock);

	switch (hint) {
		case POWER_HINT_LOW_POWER:
			if (data) {
				ALOGE("LOW_POWER_MODE is ACTIVATED\n");
			} else {
				ALOGE("LOW_POWER_MODE is DEACTIVATED\n");
			}
			erd9630->low_power_mode = data;
			break;
		case POWER_HINT_INTERACTION:
			erd9630_hint_interaction(erd9630);
			ALOGD("POWER_HINT_INTERACTION\n");
			break;
		case POWER_HINT_LAUNCH:
			if (data != nullptr)
				erd9630_hint_launch(erd9630);
			ALOGD("POWER_HINT_LAUNCH\n");
			break;
		case POWER_HINT_VSYNC:
			break;

		default:
			break;
	}

	pthread_mutex_unlock(&erd9630->lock);
}

static struct hw_module_methods_t power_module_methods = {
	.open = NULL,
};

extern "C" struct erd9630_power_module HAL_MODULE_INFO_SYM = {
	.base = {
		.common = {
			.tag = HARDWARE_MODULE_TAG,
			.module_api_version = POWER_MODULE_API_VERSION_0_2,
			.hal_api_version = HARDWARE_HAL_API_VERSION,
			.id = POWER_HARDWARE_MODULE_ID,
			.name = "ERD9630Power HAL",
			.author = "The Android Open Source Project",
			.methods = &power_module_methods,
		},
		.init = erd9630_power_init,
		.powerHint = erd9630_power_hint,
	},
	.lock = PTHREAD_MUTEX_INITIALIZER,
};
