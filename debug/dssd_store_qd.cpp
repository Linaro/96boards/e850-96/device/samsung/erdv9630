/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <log/log.h>
#include "dssd.h"

#define FILE_PATH_DIR			"/data/vendor/quickdump"
#define FILE_PATH_QD_DIR		FILE_PATH_DIR
#define FILE_PATH_QD			FILE_PATH_QD_DIR "/"
#define PATH_QD_BLK			"/dev/block/by-name/quickdump"

#define QD_METADATA_SIZE		(0x1000)
#define QD_METADATA_MAGIC		(0xDB909090)
#define QD_METADATA_OFFSET		(0x0)


#pragma pack(push, 1)
union store_qd_metadata {
	struct _qd_metadata {
		unsigned int 		magic;
		unsigned long long	qd_size;
		unsigned long long	metadata_size;
		char			file_name[512];
		unsigned long long      need_post_processing;
		unsigned long long      encrypt_start_addr;
		unsigned long long      compressed_data_size;
		unsigned long long      enc_data_size;
		unsigned long long      enc_header_offset;
		unsigned long long      enc_header_size;
	} data;
	char reserved[QD_METADATA_SIZE];
};
#pragma pack(pop)

union store_qd_metadata qd_metadata;

static void bs_dirent(struct dirent **namelist, int n) {
	int i, j;
	struct dirent *temp;
	struct stat ibuf;
	struct stat jbuf;

	for (i = 0; i < n; i++) {
		for (j = i + 1; j < n; j++) {
			stat(namelist[i]->d_name, &ibuf);
			stat(namelist[j]->d_name, &jbuf);
			if (ibuf.st_mtime > jbuf.st_mtime) {
				temp = namelist[i];
				namelist[i] = namelist[j];
				namelist[j] = temp;
			}
		}
	}
}

static void store_qd_limit_file_cnt(void) {
	char r_file_name[256];
	DIR *d;
	struct dirent *dir;
	struct dirent **namelist;
	int cnt = 0;
	int i, r_cnt;

	d = opendir(FILE_PATH_DIR);
	if (d) {
		while((dir = readdir(d)) != NULL) {
			if (dir->d_name[0] == '.')
				continue;
			cnt++;
		}
		closedir(d);
	}

	if (cnt <= 5)
		goto out;
	r_cnt = cnt - 5;

	cnt = scandir(FILE_PATH_DIR, &namelist, 0, alphasort);
	if (cnt < 0) {
		goto out;
	} else {
		bs_dirent(namelist, cnt);
		for(i = 0; i < cnt; i++) {
			if ((namelist[i]->d_name[0] != '.') && r_cnt > 0) {
				snprintf(r_file_name,
					sizeof(r_file_name),
					FILE_PATH_QD "%s",
					namelist[i]->d_name);
				remove(r_file_name);
				r_cnt--;
			}
			free(namelist[i]);
		}
		free(namelist);
	}

out:
	return;
}

int store_qd_main(int argc, char **argv) {
	char qd_file_name[512];
	FILE *qd_blk_fd;
	FILE *qd_file_fd;
	size_t copy_size;
	int fail_cnt_data = 0;
	int fail_cnt_blk = 0;
	int ret_val = 0;
	int ret;

	/* wait storage dir enabled */
	do {
		sleep(5);
		ret = access(FILE_PATH_DIR, F_OK);
		fail_cnt_data++;
		if (fail_cnt_data > 20)
			goto out;
	} while (ret);

	/* open ramdump blk dev */
	qd_blk_fd = fopen(PATH_QD_BLK, "rb");
	if (!qd_blk_fd)
		goto out;

	/* check Quickdump directory */
	ret = access(FILE_PATH_QD_DIR, F_OK);
	if (ret) {
		ret = mkdir(FILE_PATH_QD_DIR, 0777);
		if (ret) {
			ret_val = -ENODEV;
			goto out_close;
		}
	}

	/* read metadata & check data */
	file_data_read(qd_blk_fd, QD_METADATA_OFFSET, QD_METADATA_SIZE, &qd_metadata);
	if (qd_metadata.data.magic != QD_METADATA_MAGIC)
		goto out_close;

	/* alloc file buffer */
	file_buf = malloc(FILE_WRITE_UNIT);
	if (!file_buf) {
		ret_val = -ENOMEM;
		goto out_close;
	}

	/* make this quickdump data */
	snprintf(qd_file_name, sizeof(qd_file_name), FILE_PATH_QD "qd_%s.lst", qd_metadata.data.file_name);
	ret = access(qd_file_name, F_OK);
	if (!ret)
		goto out_free_close;

	qd_file_fd = fopen(qd_file_name, "wb");
	if (!qd_file_fd) {
		ret_val = -ENODEV;
		goto out_free_close;
	}

	if (qd_metadata.data.enc_data_size != 0)
		copy_size = qd_metadata.data.enc_data_size + qd_metadata.data.encrypt_start_addr;
	else if(qd_metadata.data.compressed_data_size != 0)
		copy_size = qd_metadata.data.compressed_data_size + qd_metadata.data.encrypt_start_addr;
	else
		copy_size = qd_metadata.data.qd_size;

	file_copy(qd_blk_fd, qd_file_fd, 0, 0, copy_size);
	fflush(qd_file_fd);
	fclose(qd_file_fd);

out_free_close:
	free(file_buf);
out_close:
	fclose(qd_blk_fd);
out:

	if (ret_val == 0)
		store_qd_limit_file_cnt();

	return ret_val;
}
