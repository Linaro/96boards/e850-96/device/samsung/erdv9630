/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <log/log.h>
#include "dssd.h"

#define FILE_PATH_DIR			"/data/vendor/ramdump"
#define FILE_PATH_RAMDUMP_DIR		FILE_PATH_DIR
#define FILE_PATH_RAMDUMP		FILE_PATH_RAMDUMP_DIR "/"
#define PATH_RAMDUMP_BLK		"/dev/block/by-name/ramdump"
#define FILE_ITEM_INFO			"/sys/devices/system/debug-snapshot/item_info"


#define RAMDUMP_SAVED_MAGIC		(0xCAFEBABA)
#define METADATA_OFFSET			(0)
#define METADATA_SIZE			(512 * 8)
#define RAMDUMP_OFFSET			METADATA_SIZE
#define DRAM_SIZE_2G			(0x80000000ULL)
#define DRAM_BASE			DRAM_SIZE_2G
#define DRAM_BASE2			(0x880000000ULL)
#define FILE_WRITE_UNIT			(1 * 1024 * 1024)
#define STORE_RAMDUMP_UNIT		(0x40000000ULL)
#define MAX_32bit_VALUE			(0xFFFFFFFFULL)
#define DELIM				"\n"
#define DELIM_LINE			": "

#pragma pack(push, 1)
union store_ramdump_metadata {
	struct _metadata {
		unsigned int magic;
		unsigned int flag_data_to_storage;
		unsigned long long dram_size;
		unsigned long long dram_start_addr;
		char file_name[512];
	} data;
	char reserved[METADATA_SIZE];
};
#pragma pack(pop)

struct item_information {
	char name[64];
	size_t paddr;
	size_t size;
};

union store_ramdump_metadata metadata;
struct item_information *item_info;
int item_cnt;
char file_info_buf[4096];


int store_ramdump_main(int argc, char **argv) {
	char ramdump_dir[128];
	char ramdump_file_name[512];
	FILE *ramdump_blk_fd;
	FILE *ramdump_file_fd;
	FILE *log_item_fd;
	char *ret_ptr;
	char *next_ptr;
	char *ret_ptr_string;
	char *next_ptr_string;
	unsigned long long curr_write_size;
	unsigned long long start_addr;
	unsigned long long write_unit_size;
	int fail_cnt_data = 0;
	int fail_cnt_blk = 0;
	int ret_val = 0;
	int i, ret;

	/* wait storage dir enabled */
	do {
		sleep(5);
		ret = access(FILE_PATH_DIR, F_OK);
		fail_cnt_data++;
		if (fail_cnt_data > 20)
			goto out;
	} while (ret);

	/* wait ramdump blk enabled */
	do {
		sleep(5);
		ret = access(PATH_RAMDUMP_BLK, F_OK);
		fail_cnt_blk++;
		if (fail_cnt_blk > 5)
			goto out;
	} while (ret);

	/* open ramdump blk dev */
	ramdump_blk_fd = fopen(PATH_RAMDUMP_BLK, "rb");
	if (!ramdump_blk_fd) {
		ret_val = -ENODEV;
		goto out;
	}

	/* check ramdump directory */
	ret = access(FILE_PATH_RAMDUMP_DIR, F_OK);
	if (ret) {
		ret = mkdir(FILE_PATH_RAMDUMP_DIR, 0777);
		if (ret) {
			ret_val = -ENODEV;
			goto out_close;
		}
	}

	/* read metadata & check data */
	file_data_read(ramdump_blk_fd, METADATA_OFFSET, METADATA_SIZE, &metadata);
	if (metadata.data.magic != RAMDUMP_SAVED_MAGIC)
		goto out_close;

	/* alloc file buffer */
	file_buf = malloc(FILE_WRITE_UNIT);
	if (!file_buf) {
		ret_val = -ENODEV;
		goto out_close;
	}

	/* read logging item information */
	log_item_fd = fopen(FILE_ITEM_INFO, "rb");
	if (!log_item_fd) {
		ret_val = -ENODEV;
		goto out_free_close;
	}
	fread((void *)(&file_info_buf), 1, sizeof(file_info_buf), log_item_fd);

	ret_ptr = strtok_r(file_info_buf, DELIM, &next_ptr);
	sscanf(ret_ptr, "item_count : %d", &item_cnt);
	item_info = (struct item_information *)malloc(sizeof(struct item_information) * item_cnt);

	i = 0;
	while (ret_ptr) {
		ret_ptr = strtok_r(NULL, DELIM, &next_ptr);

		if (ret_ptr) {
			ret_ptr_string = strtok_r(ret_ptr, DELIM_LINE, &next_ptr_string);
			strncpy(item_info[i].name, ret_ptr_string, strlen(ret_ptr_string));

			ret_ptr_string = strtok_r(NULL, DELIM_LINE, &next_ptr_string);
			ret_ptr_string = strtok_r(NULL, DELIM_LINE, &next_ptr_string);
			sscanf(ret_ptr_string, "%lX", &item_info[i].paddr);

			ret_ptr_string = strtok_r(NULL, DELIM_LINE, &next_ptr_string);
			ret_ptr_string = strtok_r(NULL, DELIM_LINE, &next_ptr_string);
			ret_ptr_string = strtok_r(NULL, DELIM_LINE, &next_ptr_string);
			sscanf(ret_ptr_string, "%lX", &item_info[i].size);

			i++;
		}
	}

	/* make this ramdump directory */
	snprintf(ramdump_dir, sizeof(ramdump_dir),
		FILE_PATH_RAMDUMP "%s", metadata.data.file_name);
	ret = access(ramdump_dir, F_OK);
	if (!ret)
		goto out_close_free_close;

	ret = mkdir(ramdump_dir, 0775);
	if (ret) {
		ret_val = -ENODEV;
		goto out_close_free_close;
	}

	/* save log item data */
	for (i = 0; i < item_cnt; i++) {
		snprintf(ramdump_file_name, sizeof(ramdump_file_name),
						"%s/%s_0x%llx--0x%llx.lst",
						ramdump_dir,
						item_info[i].name,
						item_info[i].paddr,
						item_info[i].paddr + item_info[i].size - 1);
		ramdump_file_fd = fopen(ramdump_file_name, "wb");
		if (!ramdump_file_fd) {
			ret_val = -ENODEV;
			goto out_close_free_close;
		}

		if (item_info[i].paddr >= DRAM_BASE2) {
			curr_write_size = DRAM_SIZE_2G + (item_info[i].paddr - DRAM_BASE2);
		} else {
			curr_write_size = item_info[i].paddr - DRAM_SIZE_2G;
		}

		file_copy(ramdump_blk_fd, ramdump_file_fd,
				RAMDUMP_OFFSET + curr_write_size,
				0, item_info[i].size);
		fflush(ramdump_file_fd);
		fclose(ramdump_file_fd);
	}

	/* save ramdump data */
	curr_write_size = 0;
	start_addr = DRAM_BASE;
	do {
		/* set file name & create file */
		if (metadata.data.dram_size - curr_write_size < STORE_RAMDUMP_UNIT)
			write_unit_size = metadata.data.dram_size - curr_write_size;
		else
			write_unit_size = STORE_RAMDUMP_UNIT;

		snprintf(ramdump_file_name, sizeof(ramdump_file_name),
			"%s/ap_0x%llx--0x%llx.lst", ramdump_dir, start_addr,
			start_addr + write_unit_size - 1);
		ramdump_file_fd = fopen(ramdump_file_name, "wb");
		if (!ramdump_file_fd) {
			ret_val = -ENODEV;
			goto out_close_free_close;
		}

		/* store ramdump file to userdata */
		file_copy(ramdump_blk_fd, ramdump_file_fd,
				RAMDUMP_OFFSET + curr_write_size,
				0, STORE_RAMDUMP_UNIT);
		fflush(ramdump_file_fd);
		fclose(ramdump_file_fd);

		/* set values for next file name */
		curr_write_size += write_unit_size;
		start_addr += write_unit_size;
		if (start_addr > MAX_32bit_VALUE)
			start_addr = DRAM_BASE2 + curr_write_size - DRAM_SIZE_2G;
	} while (curr_write_size < metadata.data.dram_size);

out_close_free_close:
	fclose(log_item_fd);
	free(item_info);
out_free_close:
	free(file_buf);
out_close:
	fclose(ramdump_blk_fd);
out:
	return ret_val;
}
