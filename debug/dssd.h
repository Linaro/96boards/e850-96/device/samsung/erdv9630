#ifndef _DEBUG_SNAPSHOT_H_
#define _DEBUG_SNAPSHOT_H_

#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

#define FILE_WRITE_UNIT		(1 * 1024 * 1024)

int store_ramdump_main(int argc, char **argv);
int store_qd_main(int argc, char **argv);
int store_diary_main(int argc, char **argv);

void file_data_read(FILE *fd, size_t offset, size_t size, void *buf);
void file_data_write(FILE *fd, size_t offset, size_t size, void *buf);
void file_copy(FILE *r_fd, FILE *w_fd, size_t r_offset, size_t w_offset, size_t size);
extern void *file_buf;

#endif
