/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <log/log.h>
#include "dssd.h"

void *file_buf;

void file_data_read(FILE *fd, size_t offset, size_t size, void *buf)
{
	size_t temp;
	size_t index = 0;

	fseek(fd, offset, SEEK_SET);

	do {
		temp = fread((void *)((unsigned long long)buf + index), 1, size - index, fd);
		index += temp;
	} while (index < size);
}

void file_data_write(FILE *fd, size_t offset, size_t size, void *buf)
{
	size_t temp;
	size_t index = 0;

	fseek(fd, offset, SEEK_SET);

	do {
		temp = fwrite((void *)((unsigned long long)buf + index), 1, size - index, fd);
		index += temp;
	} while (index < size);
}

void file_copy(FILE *r_fd, FILE *w_fd, size_t r_offset, size_t w_offset, size_t size)
{
	size_t temp = 0;
	size_t unit;

	do {
		if ((size - temp) > FILE_WRITE_UNIT)
			unit = FILE_WRITE_UNIT;
		else
			unit = size - temp;

		file_data_read(r_fd, r_offset + temp, unit, file_buf);
		file_data_write(w_fd, w_offset + temp, unit, file_buf);
		temp += unit;
	} while (temp < size);
}

