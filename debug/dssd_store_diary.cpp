/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <log/log.h>
#include "dssd.h"

#define FILE_PATH_DIR			"/data/vendor/diary"
#define FILE_PATH_DIARY_DIR		FILE_PATH_DIR
#define FILE_PATH_DIARY			FILE_PATH_DIARY_DIR "/"
#define PATH_DIARY_BLK			"/dev/block/by-name/diary"
#define PATH_DIARY_SYSFS		"/sys/devices/system/debug-snapshot/diary_enable"

#define DIARY_METADATA_SIZE		(0x1000)
#define DIARY_METADATA_MAGIC		(0xDB909090)
#define DIARY_METADATA_OFFSET		(0x0)

#pragma pack(push, 1)
union store_diary_metadata {
	struct _diary_metadata {
		unsigned int		magic;
		unsigned int		total_entry_cnt;
		unsigned int		curr_entry;
		unsigned long long 	partition_size;
		unsigned long long	entry_size;
		unsigned long long	meta_size;
		unsigned long long	sram_log_size;
		unsigned long long	dram_log_size;
		char			file_name[126];
	} data;
	char reserved[DIARY_METADATA_SIZE];
};
#pragma pack(pop)

union store_diary_metadata diary_metadata;

int store_diary_main(int argc, char **argv) {
	char diary_dir_name[128];
	char diary_file_name[512];
	FILE *diary_blk_fd;
	FILE *diary_file_fd;
	FILE *diary_sysfs_fd;
	int fail_cnt_data = 0;
	int fail_cnt_blk = 0;
	int ret_val = 0;
	int i, ret;
	int total_cnt;
	size_t entry_size;
	size_t meta_size;
	size_t sram_log_size;
	size_t dram_log_size;
	size_t offset;

	/* wait storage dir enabled */
	do {
		sleep(5);
		ret = access(FILE_PATH_DIR, F_OK);
		fail_cnt_data++;
		if (fail_cnt_data > 20)
			goto out;
	} while (ret);

	/* open sysfs node & stop diary logging */
	diary_sysfs_fd = fopen(PATH_DIARY_SYSFS, "wb");
	if (!diary_sysfs_fd)
		goto out;

	fwrite("0", 1, 1, diary_sysfs_fd);

	/* open diary blk */
	diary_blk_fd = fopen(PATH_DIARY_BLK, "rb");
	if (!diary_blk_fd)
		goto out_close;

	/* check Diary directory */
	ret = access(FILE_PATH_DIARY_DIR, F_OK);
	if (ret) {
		ret = mkdir(FILE_PATH_DIARY_DIR, 0777);
		if (ret) {
			ret_val = -ENODEV;
			goto out_close2;
		}
	}

	/* read metadata & check data */
	file_data_read(diary_blk_fd, DIARY_METADATA_OFFSET, DIARY_METADATA_SIZE, &diary_metadata);
	if (diary_metadata.data.magic != DIARY_METADATA_MAGIC)
		goto out_close2;

	/* alloc file_buffer */
	file_buf = malloc(FILE_WRITE_UNIT);
	if (!file_buf) {
		ret_val = -ENOMEM;
		goto out_close2;
	}

	/* storing each entry */
	total_cnt = diary_metadata.data.total_entry_cnt;
	meta_size = diary_metadata.data.meta_size;
	entry_size = diary_metadata.data.entry_size;
	sram_log_size = diary_metadata.data.sram_log_size;
	dram_log_size = diary_metadata.data.dram_log_size;

	for (i = 0; i < total_cnt; i++) {
		/* read metadata */
		offset = entry_size * i;
		file_data_read(diary_blk_fd, offset, meta_size,
						&diary_metadata);
		if (diary_metadata.data.magic != DIARY_METADATA_MAGIC)
			continue;

		/* check data existed */
		snprintf(diary_dir_name, sizeof(diary_dir_name),
			 		FILE_PATH_DIARY "%s",
					diary_metadata.data.file_name);

		ret = access(diary_dir_name, F_OK);
		if (!ret)
			continue;

		/* make new data's directory */
		ret = mkdir(diary_dir_name, 0755);
		if (ret) {
			ret_val = -ENODEV;
			goto out_free_close;
		}

		/* save sram & dram data */
		snprintf(diary_file_name, sizeof(diary_file_name),
					"%s/sram.lst", diary_dir_name);
		diary_file_fd = fopen(diary_file_name, "wb");
		if (!diary_file_fd) {
			ret_val = -ENODEV;
			goto out_free_close;
		}

		offset += meta_size;
		file_copy(diary_blk_fd, diary_file_fd,
				offset, 0, sram_log_size);
		fflush(diary_file_fd);
		fclose(diary_file_fd);

		snprintf(diary_file_name, sizeof(diary_file_name),
					"%s/dram.lst", diary_dir_name);
		diary_file_fd = fopen(diary_file_name, "wb");
		if (!diary_file_fd) {
			ret_val = -ENODEV;
			goto out_free_close;
		}

		offset += sram_log_size;
		file_copy(diary_blk_fd, diary_file_fd,
				offset, 0, dram_log_size);
		fflush(diary_file_fd);
		fclose(diary_file_fd);
	}

out_free_close:
	free(file_buf);
out_close2:
	fclose(diary_blk_fd);
out_close:
	fclose(diary_sysfs_fd);
out:
	return ret_val;
}
