LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
#$(info build customer command)
LOCAL_SRC_FILES := spad.json
LOCAL_MODULE := spad
LOCAL_MODULE_OWNER := samsung
LOCAL_MODULE_SUFFIX := .json
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := optional
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
#$(info build customer command)
LOCAL_SRC_FILES := sfmd.json
LOCAL_MODULE := sfmd
LOCAL_MODULE_OWNER := samsung
LOCAL_MODULE_SUFFIX := .json
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := optional
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
#$(info build customer command)
LOCAL_SRC_FILES := sctd.json
LOCAL_MODULE := sctd
LOCAL_MODULE_OWNER := samsung
LOCAL_MODULE_SUFFIX := .json
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := optional
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
#$(info build customer command)
LOCAL_SRC_FILES := swcnd.json
LOCAL_MODULE := swcnd
LOCAL_MODULE_OWNER := samsung
LOCAL_MODULE_SUFFIX := .json
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := optional
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)
