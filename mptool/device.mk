#
# Copyright Samsung Electronics Co., LTD.
#
# This software is proprietary of Samsung Electronics.
# No part of this software, either material or conceptual may be copied or distributed, transmitted,
# transcribed, stored in a retrieval system or translated into any human or computer language in any form by any means,
# electronic, mechanical, manual or otherwise, or disclosed
# to third parties without the express written permission of Samsung Electronics.
#

#TARGET_USE_FACTORY_IMAGES := true
#ifeq ($(TARGET_USE_FACTORY_IMAGES), true)
#TARGET_SUPPORT_MPTOOL_IN_NORMAL_MODE := true
#endif

#BOARD_USE_SSCR_TOOLS += pcba modem wcn console fm

BOARD_USE_SSCR_TOOLS := slogkit
ifeq ($(TARGET_USE_FACTORY_IMAGES),true)
##for mptoool configuration
BOARD_USE_SSCR_TOOLS += pcba modem wcn console fm
endif

# sLogReports.apk
TARGET_USE_LOG_OWNER := samsung

$(call inherit-product-if-exists, vendor/samsung_slsi/sscr_tools/device.mk)

ifeq ($(TARGET_USE_FACTORY_IMAGES), true)
ifneq (,$(filter pcba, $(BOARD_USE_SSCR_TOOLS)))
PRODUCT_PACKAGES += \
        libspad_hal \
	spad.json
endif

ifneq (,$(filter wcn, $(BOARD_USE_SSCR_TOOLS)))
PRODUCT_PACKAGES += \
        libwlbt \
	swcnd.json
endif

ifneq (,$(filter fm, $(BOARD_USE_SSCR_TOOLS)))
PRODUCT_PACKAGES += \
	sfmd.json
endif

ifneq (,$(filter console, $(BOARD_USE_SSCR_TOOLS)))
PRODUCT_PACKAGES += \
	sctd.json
endif

ifneq (,$(filter pcba modem wcn console fm, $(BOARD_USE_SSCR_TOOLS)))
current_dir := $(dir $(lastword $(MAKEFILE_LIST)))
$(warning current mptool dir: $(current_dir))

PRODUCT_COPY_FILES += \
	$(current_dir)conf/init.factory.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.factory.rc \
	$(current_dir)conf/fstab.ufs:$(TARGET_COPY_OUT_VENDOR)/etc/init/fstab.factory

ifeq ($(TARGET_SUPPORT_MPTOOL_IN_NORMAL_MODE), true)
PRODUCT_COPY_FILES += \
	$(current_dir)conf/init.normal.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.snormal.rc
endif

ifeq ($(TARGET_SUPPORT_LOG_IN_FACTORY_MODE), true)
PRODUCT_COPY_FILES += \
	$(dir $(TARGET_BOARD_INFO_FILE))mptool/conf/fstab.exynos9630:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
endif
endif
endif

