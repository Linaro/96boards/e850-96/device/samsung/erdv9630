/*
 * Copyright 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "android.hardware.light@2.0-test"

#include <android/hardware/light/2.0/ILight.h>
#include <hidl/LegacySupport.h>
#include <utils/Log.h>
#include <utils/RefBase.h>
#include <android/hardware/light/2.0/ILight.h>
#include <android/hardware/light/2.0/types.h>
#include <stdio.h>
#include "backlight.h"

using android::hardware::light::V2_0::ILight;
using android::hardware::defaultPassthroughServiceImplementation;
using android::hardware::light::V2_0::LightState;
using android::hardware::light::V2_0::Type;
using Status     = ::android::hardware::light::V2_0::Status;
using Type       = ::android::hardware::light::V2_0::Type;
template<typename T>
using Return     = ::android::hardware::Return<T>;

static int processReturn(const Return<Status> &ret, Type type)
{
    if (!ret.isOk())
    {
        ALOGE("Failed to issue set light command.");
        return -1;
    }

    switch (static_cast<Status>(ret))
    {
        case Status::SUCCESS:
            return 0;
        case Status::LIGHT_NOT_SUPPORTED:
            ALOGE("Light requested not available on this device. %d", type);
            return -1;
        case Status::UNKNOWN:
        default:
            ALOGE("Unknown error setting light.");
            return -1;
    }
}

int setBacklight(int color)
{
    static android::sp<ILight> sLight;
    LightState ls;

    Type type = static_cast<Type>(0);
    ls.color = color;
    sLight = ILight::getService();
    if (sLight == nullptr)
    {
        ALOGE("Unable to get ILight interface.");
        return -1;
    }
    Return<Status> val = sLight->setLight(type, ls);
    return processReturn(val, type);
}
