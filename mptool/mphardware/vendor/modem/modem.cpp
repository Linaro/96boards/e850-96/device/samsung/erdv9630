/*
 * Copyright Samsung Electronics Co., LTD.
 *
 * This software is proprietary of Samsung Electronics.
 * No part of this software, either material or conceptual may be copied or distributed, transmitted,
 * transcribed, stored in a retrieval system or translated into any human or computer language in any form by any
means,
 * electronic, mechanical, manual or otherwise, or disclosed
 * to third parties without the express written permission of Samsung Electronics.
 */

/*
    Response service class implementation
*/
#include "modem.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "log/log.h"
#include "cutils/sockets.h"
#include "data.h"

#define LOG_TAG    "SPAD"


static uint8_t gResBuffer[MAX_FACTORY_CMD_PARAM_SIZE];
#define SOCKET_SYNC_SMCD "/dev/socket/sync_smcd"

int SendSyncATCommand(const uint8_t *cmd, int length, uint8_t **resp, int *resplen)
{
    int err = -1;
    int cnt = 0;
    int ret = HAL_E_COMM_ERR;
    fd_set fdset;
    *resp = (uint8_t *)gResBuffer;
    memset(*resp, 0, MAX_FACTORY_CMD_PARAM_SIZE);

    int fd = socket_local_client(SOCKET_SYNC_SMCD, ANDROID_SOCKET_NAMESPACE_ABSTRACT, SOCK_STREAM);
    if(fd < 0)
    {
        ALOGE("%s : open socket(%s) fail(%d)\n", __func__, SOCKET_SYNC_SMCD, errno);
        return ret;
    }
    send(fd, (const void *)cmd, length, 0);
    //receive the at response
    while (1)
    {
        FD_ZERO(&fdset);
        FD_SET(fd, &fdset);
        struct timeval timeout = {20, 0};
        err = select(fd + 1, &fdset, NULL, NULL, &timeout);
        if (err < 0)
        {
            if (errno == EINTR)
            {
                continue;
            }
            ALOGE("%s : select err = %d\n", __func__, errno);
            break;
        }
        else if (err == 0)
        {
            ALOGW("%s:: timeout.\n", __func__);
            ret = HAL_E_TIMEOUT;
            break;
        }

        if (FD_ISSET(fd, &fdset))
        {
            cnt = read(fd, *resp, MAX_FACTORY_CMD_PARAM_SIZE);
            if (cnt < 0)
            {
                if(errno == EINTR || errno == EAGAIN)
                {
                    continue;
                }
                ALOGE("%s : Fail to read %s, error = %d\n", __FUNCTION__, SOCKET_SYNC_SMCD, errno);
            }
            else if (cnt == 0)
            {
                ALOGE("%s : No data on %s\n", __FUNCTION__, SOCKET_SYNC_SMCD);
                break;
            }
            ALOGD("%s : recive size(%d)\n", __FUNCTION__, cnt);
            ret = HAL_E_SUCCESS;
            break;
        }
    }
    close(fd);
    *resplen = cnt;
    return ret;
}

#define MAX_ERR_CODE_NUM  10
int CheckResultByResp(string strResp)
{
    string errStr[MAX_ERR_CODE_NUM] = {"NG", "error", "invalid"};
    size_t pos = string::npos;
    int ret = HAL_E_ERR;

    for(int i = 0; i < MAX_ERR_CODE_NUM; i++)
    {
        if(errStr[i].empty())
        {
            break;
        }
        if((pos = strResp.find(errStr[i])) != string::npos)
        {
            return ret;
        }
    }
    return 0;
}


