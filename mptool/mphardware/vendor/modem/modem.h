/*
 * Copyright Samsung Electronics Co., LTD.
 *
 * This software is proprietary of Samsung Electronics.
 * No part of this software, either material or conceptual may be copied or distributed, transmitted,
 * transcribed, stored in a retrieval system or translated into any human or computer language in any form by any
means,
 * electronic, mechanical, manual or otherwise, or disclosed
 * to third parties without the express written permission of Samsung Electronics.
 */

/*
    MODEM COMMAND class
*/
#ifndef __MODEM_COMMAND_H_
#define __MODEM_COMMAND_H_

#include <stdint.h>
#include <string>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif


int SendSyncATCommand(const uint8_t* cmd, int length, uint8_t** resp, int* resplen);
int CheckResultByResp(string strResp);


#ifdef __cplusplus
}
#endif
#endif


