#ifndef __VENDOR_NV_TEST_H__
#define __VENDOR_NV_TEST_H__

#include <string>
#include <stdio.h>
#include <stdlib.h>
//#include <android/log.h>
#define LOG_TAG    "SPAD"
#include <log/log.h>

#ifdef __cplusplus
extern "C" {
#endif

#define READ_CMD	0
#define WRITE_CMD	1
#define VERIFY_CMD	2

typedef enum {
	OPERATE_TYPE_FILE,
	OPERATE_TYPE_IOCTL,
	OPERATE_TYEPE_SYSCALL,
} OptTypeE;

typedef struct {
	unsigned int slot;
	int offset;
	/*read operate:
		size = 0 - get whole slot data size;
		size != 0, read the special size
	write operate:
		write size*/
	int size;
	unsigned char data[1024];
} NVItem;

class NVFactoryTest
{
public:
	NVFactoryTest();
	virtual ~NVFactoryTest();
	static NVFactoryTest* Instance();
	static void Exitance();
	virtual int RDWRNVItem(int opt, NVItem& item, OptTypeE type = OPERATE_TYPE_FILE);
	virtual int RDWRNVItem(int opt, std::string name, NVItem& item, OptTypeE type = OPERATE_TYPE_FILE);
	virtual int VerifyHOB(OptTypeE type = OPERATE_TYPE_FILE);
};

#ifdef __cplusplus
}
#endif
#endif
