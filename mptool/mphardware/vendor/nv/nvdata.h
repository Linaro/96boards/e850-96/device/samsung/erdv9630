#ifndef __VENDOR_NV_DATA_H__
#define __VENDOR_NV_DATA_H__

#include <string>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	std::string nvname;
	std::string partition;
	unsigned int slot;
	unsigned int start;
	unsigned int offset;
	unsigned int nvsize;
	unsigned int blocksize;
	unsigned int magic;
} NVData;

#define NV_CSUN_LENGTH	32
//#define NV_PARTITION	"/dev/block/platform/13600000.ufs/by-name/proinfo"
//#define NVBK_PARTITION	"/dev/block/platform/13600000.ufs/by-name/proinfo_backup"
#define NV_PARTITION	"/mnt/vendor/proinfo"
#define NVBK_PARTITION	"/mnt/vendor/proinfobk"


/*Track ID(Serial Number)*/
#define SLOT_INDEX_TID			0
#define SLOT_INDEX_MD5			1

static NVData NVList[] = {
	/*	nvname		partition		slot				start	offset	nvsize	blocksize	magic*/
	{"PSN", 	NV_PARTITION,	SLOT_INDEX_TID, 	0,		0,		30, 	201,		SLOT_INDEX_MD5},
	{"FSN", 	NV_PARTITION,	SLOT_INDEX_TID, 	0,		30, 	30, 	201,		SLOT_INDEX_MD5},
	{"MD5", 	NV_PARTITION,	SLOT_INDEX_MD5, 	512,	0,		32, 	512,		0}
};

struct NVBackup {
	std::string name;
	int cplen;
};

static struct NVBackup NVHob = {NVBK_PARTITION, 1024};

#ifdef __cplusplus
}
#endif
#endif
