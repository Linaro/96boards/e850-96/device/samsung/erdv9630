#ifndef __VENDOR_NV__OPERATE_H__
#define __VENDOR_NV__OPERATE_H__

#include "nvtest.h"

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "nvdata.h"
#include "nvtest.h"

#ifdef __cplusplus
extern "C" {
#endif

class NVTest
{
public:
	NVTest();
	virtual ~NVTest();
	static NVTest* Instance(OptTypeE type);
	static void Exitance();
	virtual int RDWRNVItem(int opt, std::string name, NVItem& item);
	virtual int RDWRNVItem(int opt, NVItem& item);
	virtual int VerifyHOBCrypto();

	virtual int ReadNV(NVItem& item) = 0;
	virtual int WriteNV(NVItem& item) = 0;
};

class NVFileOpt : public NVTest
{
public:
	NVFileOpt();
	virtual ~NVFileOpt();
	virtual int ReadNV(NVItem& item);
	virtual int WriteNV(NVItem& item);
	virtual int CreateCrypto(int fd, NVData & data);
	virtual int VerifyCrypto(int fd, NVData & data);
	int CalcCrypto(char* csum, const char* buf, int len);
	virtual int VerifyHOBCrypto();
	int InitHOB();
};

class NVSysCallOpt : public NVTest
{
public:
	NVSysCallOpt();
	virtual ~NVSysCallOpt();
	virtual int ReadNV(NVItem& item);
	virtual int WriteNV(NVItem& item);
};

class NVIOCtlOpt : public NVTest
{
public:
	NVIOCtlOpt();
	virtual ~NVIOCtlOpt();
	virtual int ReadNV(NVItem& item);
	virtual int WriteNV(NVItem& item);
};

#ifdef __cplusplus
}
#endif
#endif
