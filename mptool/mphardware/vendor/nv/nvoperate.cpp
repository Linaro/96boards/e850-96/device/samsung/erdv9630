#include "nvoperate.h"
#include "nvdata.h"
//#include <sys/ioctl.h>
#include <openssl/md5.h>
#include <unistd.h>
#include <errno.h>

static NVFileOpt *gpNVFile = nullptr;
static NVSysCallOpt *gpNVSyscall = nullptr;
static NVIOCtlOpt *gpNVIOCtl = nullptr;

//const char * gpSignature = "SAMSUNG_MPTOOL_NV";
static bool isVerifyHob = false;

NVTest::NVTest()
{
}

NVTest::~NVTest()
{

}

NVTest *NVTest::Instance(OptTypeE type)
{
    NVTest *pNV = nullptr;
    switch(type)
    {
        case OPERATE_TYPE_FILE:
            if(gpNVFile == nullptr)
            {
                gpNVFile = new NVFileOpt();
            }
            pNV = gpNVFile;
            break;
        case OPERATE_TYEPE_SYSCALL:
            if(gpNVSyscall == nullptr)
            {
                gpNVSyscall = new NVSysCallOpt();
            }
            pNV = gpNVSyscall;
            break;
        case OPERATE_TYPE_IOCTL:
            if(gpNVIOCtl == nullptr)
            {
                gpNVIOCtl = new NVIOCtlOpt();
            }
            pNV = gpNVIOCtl;
            break;
    }
    return pNV;
}

void NVTest::Exitance()
{
    if(gpNVFile == nullptr)
    {
        delete gpNVFile;
        gpNVFile = nullptr;
    }
    if(gpNVSyscall == nullptr)
    {
        delete gpNVSyscall;
        gpNVSyscall = nullptr;
    }
    if(gpNVIOCtl == nullptr)
    {
        delete gpNVIOCtl;
        gpNVIOCtl = nullptr;
    }
}

int NVTest::RDWRNVItem(int opt, NVItem &item)
{
    if (opt == READ_CMD)
    {
        ALOGD("NVTest::RDWRNVItem:: read nv item");
        return ReadNV(item);
    }
    else if(opt == WRITE_CMD)
    {
        ALOGD("NVTest::RDWRNVItem:: write nv item");
        return WriteNV(item);
    }
    ALOGE("NVTest::RDWRNVItem:: error operate id (%d)", opt);
    return 1;
}

int NVTest::RDWRNVItem(int opt, std::string name, NVItem &item)
{
    for(unsigned int i = 0; i < sizeof(NVList) / sizeof(NVList[0]); i++)
    {
        if(NVList[i].nvname == name)
        {
            ALOGD("NVTest::RDWRNVItem:: find the right nv item. operate id(%d)", opt);
            item.slot = NVList[i].slot;
            item.offset = NVList[i].offset;
            if(opt == READ_CMD)
            {
                item.size = NVList[i].nvsize;
                return ReadNV(item);
            }
            else if(opt == WRITE_CMD)
            {
                return WriteNV(item);
            }
        }
    }
    ALOGE("NVTest::RDWRNVItem:: no exist the right nv item");
    return 1;
}

int NVTest::VerifyHOBCrypto()
{
    ALOGE("NVTest::%s::default is fail", __FUNCTION__);
    return 1;
}

NVFileOpt::NVFileOpt()
{
    InitHOB();
}

NVFileOpt::~NVFileOpt()
{

}

int NVFileOpt::InitHOB()
{
    if(!isVerifyHob)
    {
        isVerifyHob = true;
        char csum[NV_CSUN_LENGTH + 1];
        memset(csum, 0, (NV_CSUN_LENGTH + 1));
        int fd = open(NV_PARTITION, O_RDWR | O_SYNC);
        if(fd == -1)
        {
            ALOGE("NVFileOpt::%s::open(%s) fail", __FUNCTION__, NV_PARTITION);
            isVerifyHob = false;
            return 1;
        }
        off_t cur = lseek(fd, NVList[SLOT_INDEX_MD5].start, SEEK_SET);
        ALOGD("NVFileOpt::%s::seek hob(%d)", __FUNCTION__, cur);
        read(fd, csum, NV_CSUN_LENGTH);
        if(csum[0] == 0)
        {
            ALOGD("NVFileOpt::%s::no md5, create new md5 value", __FUNCTION__);
            //no md5, create
            CreateCrypto(fd, NVList[2]);
        }
        close(fd);
        fd = -1;
    }
    return 0;
}

int NVFileOpt::ReadNV(NVItem &item)
{
    unsigned int slot_offset = 0;
    ALOGD("NVFileOpt::ReadNV:: slot(%d)", item.slot);
    if(InitHOB() != 0)
    {
        ALOGE("NVFileOpt::ReadNV:: verify hob fail");
        return 1;
    }
    for(unsigned int i = 0; i < sizeof(NVList) / sizeof(NVList[0]); i++)
    {
        if(NVList[i].slot != item.slot)
        {
            continue;
        }
        slot_offset = NVList[i].start + item.offset;
        if(item.size == 0)
        {
            item.size = NVList[i].blocksize - item.offset;
        }

        int fd = open(NVList[i].partition.c_str(), O_RDONLY);
        if(fd == -1)
        {
            ALOGE("NVFileOpt::ReadNV:: open file(%s) fail", NVList[i].partition.c_str());
            return 1;
        }

        if(VerifyCrypto(fd, NVList[i]) != 0)
        {
            ALOGE("NVFileOpt::ReadNV:: verify(%s) fail", NVList[i].partition.c_str());
            close(fd);
            return 1;
        }

        off_t cur = lseek(fd, slot_offset, SEEK_SET);
        if(cur == -1)
        {
            ALOGE("NVFileOpt::ReadNV:: seek file(%s) fail", NVList[i].partition.c_str());
            close(fd);
            return 1;
        }
        ssize_t len = read(fd, item.data, item.size);
        ALOGD("NVFileOpt::ReadNV:: offset(%d), size(%d), real size(%d)", slot_offset, item.size, len);
        close(fd);
        if(len <= 0)
        {
            ALOGE("NVFileOpt::ReadNV:: read nv item error");
            return 1;
        }
        int j = 0;
        for(j = 0; j < len; j++)
        {
            if((item.data)[j] != '\0')
            {
                break;
            }
        }
        if(j == len)
        {
            ALOGE("NVFileOpt::ReadNV:: no nv data");
            return 2;
        }
        item.size = len;
        return 0;
    }
    ALOGE("NVFileOpt::ReadNV:: NO exist nv item");
    return 1;
}

int NVFileOpt::WriteNV(NVItem &item)
{
    if(item.size == 0)
    {
        ALOGE("NVFileOpt::WriteNV:: nv data size is zero");
        return 1;
    }
    ALOGD("NVFileOpt::WriteNV:: slot(%d)", item.slot);
    unsigned int slot_offset = 0;
    int ret = -1;

    for(unsigned int i = 0; i < sizeof(NVList) / sizeof(NVList[0]); i++)
    {
        if(NVList[i].slot != item.slot)
        {
            continue;
        }
        int fd = open(NVList[i].partition.c_str(), O_RDWR | O_SYNC);
        if(fd == -1)
        {
            ALOGE("NVFileOpt::WriteNV:: open file(%s) faile", NVList[i].partition.c_str());
            return 1;
        }

        slot_offset = NVList[i].start + item.offset;

        off_t cur = lseek(fd, slot_offset, SEEK_SET);
        if(cur == -1)
        {
            ALOGE("NVFileOpt::WriteNV:: seek file(%s) faile", NVList[i].partition.c_str());
            close(fd);
            return 1;
        }

        unsigned char buf[item.size + 1];
        memset(buf, 0, item.size + 1);
        read(fd, buf, item.size);

        cur = lseek(fd, slot_offset, SEEK_SET);

        ALOGD("NVFileOpt::WriteNV:: offset(%d), size(%d)", slot_offset, item.size);
        ret = write(fd, item.data, item.size);

        if(ret < 0)
        {
            ALOGE("NVFileOpt::write backup error(%s)", strerror(errno));
            close(fd);
            return 1;
        }
        ret = CreateCrypto(fd, NVList[i]);
        if(ret == 0)
        {
            //save to bakcup nv
            int fdbk = open(NVHob.name.c_str(), O_RDWR | O_SYNC);
            if(fdbk == -1)
            {
                ALOGE("NVFileOpt::Save to HOB:: open file(%s) faile", NVHob.name.c_str());
                close(fd);
                return 1;
            }

            slot_offset = NVList[i].start + item.offset;

            off_t curbk = lseek(fdbk, slot_offset, SEEK_SET);
            if(curbk == -1)
            {
                ALOGE("NVFileOpt::WriteNV:: seek file(%s) faile", NVHob.name.c_str());
                close(fd);
                close(fdbk);
                return 1;
            }

            unsigned char buf[item.size + 1];
            memset(buf, 0, item.size + 1);
            read(fdbk, buf, item.size);

            cur = lseek(fdbk, slot_offset, SEEK_SET);

            ALOGD("NVFileOpt::WriteNVHOB:: offset(%d), size(%d)", slot_offset, item.size);
            ret = write(fdbk, item.data, item.size);

            if(ret < 0)
            {
                ALOGE("NVFileOpt::write HOB error(%s)", strerror(errno));
                close(fd);
                close(fdbk);
                return 1;
            }
            ret = CreateCrypto(fdbk, NVList[i]);
            close(fdbk);
        }

        close(fd);

        return ret;
    }
    ALOGE("NVFileOpt::WriteNV:: NO exist nv item");
    return 1;
}

int NVFileOpt::CalcCrypto(char *csum, const char *buf, int len)
{
    MD5_CTX md5;
    MD5_Init(&md5);

    unsigned char digest[16] = {0};

    MD5_Update(&md5, buf, len);

    const char *signature = "SAMSUNG_MPTOOL_NV";
    MD5_Update(&md5, signature, strlen(signature));
    MD5_Final(digest, &md5);

    // done
    for (int i = 0; i < 16; i++)
    {
        snprintf(csum + (i * 2), 3, "%02x", digest[i]);
    }
    return 0;
}

int NVFileOpt::CreateCrypto(int fd, NVData &data)
{
    off_t cur = lseek(fd, data.start, SEEK_SET);
    if(cur == -1)
    {
        ALOGE("NVFileOpt::CreateCrypto:: seek file(%s) faile", strerror(errno));
        return 1;
    }
    unsigned char *buf = (unsigned char *)malloc(data.blocksize + 1);
    memset(buf, 0, data.blocksize + 1);
    //ALOGD("NVFileOpt::CreateCrypto:: file(%s), start(%d) block size(%d)", data.partition.c_str(), data.start, data.blocksize);

    int len = read(fd, (void *)buf, data.blocksize);
    if(len < 0)
    {
        ALOGE("NVFileOpt::CreateCrypto:: read file error(%s)", strerror(errno));
        free(buf);
        return 1;
    }
    //calc md5 data
    char csum[NV_CSUN_LENGTH + 1];
    memset(csum, 0, (NV_CSUN_LENGTH + 1));
    //ALOGD("NVFileOpt::CreateCrypto:: read nv size(%d)", len);
    if(CalcCrypto(csum, (const char *)buf, data.blocksize) != 0)
    {
        ALOGE("NVFileOpt::CreateCrypto:: calc crypto error");
        free(buf);
        return 1;
    }
    free(buf);

    //write new md5 data
    for(unsigned int i = 0; i < sizeof(NVList) / sizeof(NVList[0]); i++)
    {
        if(NVList[i].slot != data.magic)
        {
            continue;
        }
        int offset = NVList[i].start + NVList[i].offset;
        //ALOGD("NVFileOpt::CreateCrypto:: write md5(%s)", csum);
        cur = lseek(fd, offset, SEEK_SET);
        if(cur == -1)
        {
            ALOGE("NVFileOpt::CreateCrypto:: seek md5 file error(%s) ", strerror(errno));
            close(fd);
            return 1;
        }
        write(fd, csum, NV_CSUN_LENGTH);
        break;
    }
    return 0;
}

int NVFileOpt::VerifyHOBCrypto()
{
    int fdbk = open(NVHob.name.c_str(), O_RDWR | O_SYNC);
    if(fdbk == -1)
    {
        ALOGE("NVFileOpt::Verify HOB:: open file(%s) faile", NVHob.name.c_str());
        return 1;
    }
    ALOGD("NVFileOpt::(%s)::Verify HOB:: Verify FTI partition", __FUNCTION__);
    VerifyCrypto(fdbk, NVList[1]);

    close(fdbk);
    return 0;
}

int NVFileOpt::VerifyCrypto(int fd, NVData &data)
{
    //create md5 data
    char csum[NV_CSUN_LENGTH + 1];
    memset(csum, 0, (NV_CSUN_LENGTH + 1));
    unsigned char *buf = (unsigned char *)malloc(data.blocksize + 1);
    memset(buf, 0, data.blocksize + 1);
    lseek(fd, data.start, SEEK_SET);
    read(fd, buf, data.blocksize);
    if(CalcCrypto(csum, (const char *)buf, data.blocksize) != 0)
    {
        ALOGE("NVFileOpt::VerifyCrypto:: calc crypto error");
        free(buf);
        return 1;
    }
    free(buf);
    //read exist md5 data
    char oldsum[NV_CSUN_LENGTH + 1];
    memset(oldsum, 0, (NV_CSUN_LENGTH + 1));
    for(unsigned int i = 0; i < sizeof(NVList) / sizeof(NVList[0]); i++)
    {
        if(NVList[i].slot != data.magic)
        {
            continue;
        }
        int offset = NVList[i].start + NVList[i].offset;
        lseek(fd, offset, SEEK_SET);
        read(fd, oldsum, NV_CSUN_LENGTH);
        break;
    }

    //compare md5 data
    int ret = 1;
    if(memcmp(csum, oldsum, NV_CSUN_LENGTH) == 0)
    {
        ALOGD("NVFileOpt::VerifyCrypto:: MD5 match");
        ret = 0;
    }
    else
    {
        ALOGE("NVFileOpt::VerifyCrypto:: MD5 not match");
        ret = 1;
    }
    return ret;
}

NVSysCallOpt::NVSysCallOpt()
{
}

NVSysCallOpt::~NVSysCallOpt()
{
}

int NVSysCallOpt::ReadNV(NVItem &item)
{
    return 1;
}

int NVSysCallOpt::WriteNV(NVItem &item)
{
    return 1;
}


NVIOCtlOpt::NVIOCtlOpt()
{
}

NVIOCtlOpt::~NVIOCtlOpt()
{
}

int NVIOCtlOpt::ReadNV(NVItem &item)
{
    return 1;
}

int NVIOCtlOpt::WriteNV(NVItem &item)
{
    return 1;
}



