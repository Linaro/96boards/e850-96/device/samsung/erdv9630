#include "nvtest.h"

#include "nvdata.h"
#include "nvoperate.h"
NVFactoryTest *gpFactoryTest = nullptr;

NVFactoryTest::NVFactoryTest()
{
}

NVFactoryTest::~NVFactoryTest()
{
}

NVFactoryTest *NVFactoryTest::Instance()
{
    if(gpFactoryTest == nullptr)
    {
        gpFactoryTest = new NVFactoryTest();
    }
    return gpFactoryTest;
}

void NVFactoryTest::Exitance()
{
    NVTest::Exitance();

    if(gpFactoryTest != nullptr)
    {
        delete gpFactoryTest;
    }
    gpFactoryTest = nullptr;
}

int NVFactoryTest::RDWRNVItem(int opt, NVItem &item, OptTypeE type)
{
    ALOGD("NVFactoryTest::RDWRNVItem by slot. operate(%d), type(%d)", opt, type);
    return NVTest::Instance(type)->RDWRNVItem(opt, item);
}

int NVFactoryTest::RDWRNVItem(int opt, std::string name, NVItem &item, OptTypeE type)
{
    ALOGD("NVFactoryTest::RDWRNVItem by name(%s). operate(%d), type(%d)", name.c_str(), opt, type);
    return NVTest::Instance(type)->RDWRNVItem(opt, name, item);
}

int NVFactoryTest::VerifyHOB(OptTypeE type)
{
    ALOGD("NVFactoryTest::VerifyHOB type(%d)", type);
    return NVTest::Instance(type)->VerifyHOBCrypto();
}

