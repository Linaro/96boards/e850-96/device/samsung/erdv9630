LOCAL_PATH:= $(call my-dir)


##################################################
include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/include

LOCAL_SRC_FILES := functions.cpp vendor.cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/vendor
LOCAL_SRC_FILES += vendor/backlight/backlight.cpp \
                   vendor/modem/modem.cpp


###nv operate
LOCAL_SRC_FILES += vendor/nv/nvoperate.cpp \
		vendor/nv/nvtest.cpp

LOCAL_SHARED_LIBRARIES += libcrypto


LOCAL_SHARED_LIBRARIES += \
    libhardware \
    libhardware_legacy \
    libcutils \
    libutils \
    libtinyalsa \
    libc \
    libz \
    libbase \
    liblog \
    libz \
    libutils \
    libfmq \
    libstagefright_foundation

#    libext4_utils \
#    libsparse \
#    libandroid \

LOCAL_SHARED_LIBRARIES += \
    libhidlbase \
    libhidltransport \
    android.hardware.light@2.0 \

LOCAL_LDLIBS := -llog
LOCAL_CFLAGS := -Wall -Wno-unused-parameter
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:= libspad_hal
include $(BUILD_SHARED_LIBRARY)
##################################################

include $(call all-makefiles-under,$(LOCAL_PATH))
