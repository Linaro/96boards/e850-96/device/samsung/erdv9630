#ifndef __ENGMODE_VENDOR_H__
#define __ENGMODE_VENDOR_H__

#define LOG_TAG    "SPAD"
#include <android/log.h>

#include <cutils/properties.h>
#include <log/log.h>

#include <stdlib.h>
#include <stdio.h>
#include <android-base/properties.h>
#include <string.h>
#include <unistd.h>

#include "data.h"

#define AT_OK_RESPONSE      "\r\nOK\r\n"
#define AT_ERROR_RESPONSE      "\r\nERROR\r\n"
#define AT_CMD_ENG_SYMBOL   "\r\n"
#define COLON_SYMBOL        ":"
using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	REBOOT_HALT = 0,
	REBOOT_FACTORY = 1,
	REBOOT_NORMAL = 2,
	REBOOT_RECOVERY = 3,
	REBOOT_FASTBOOT = 4,
} RebootModeE;

typedef enum {
	EVENT_START = 0,
	EVENT_END   = 1,
	EVENT_READ  = 2,
} CommenEventE;

int closeAdb();
int openAdb();

int getChipId(SFCmdS *value);

void reboot(int mode);

int getVersion(SFCmdS* value);

int systemSleep();

int readBattery(SFCmdS *value);

int getStorage(SFCmdS* value);

int getMemory(SFCmdS* value);

int getSDCard(SFCmdS* value);

int getMPVersion(SFCmdS* value);

int getMPName(SFCmdS* value);

int getHWID(SFCmdS* value);

int getHWVersion(SFCmdS* value);

int getCPUTemp(SFCmdS* value);

int getChargerTemp(SFCmdS* value);

int getPATemp(SFCmdS* value);

int getBatteryTemp(SFCmdS* value);

int earsePartition(char *partition, SFCmdS* value);

int audioCmd(const char* cmd, SFCmdS* value);

int testBackLight(int value);

int gyroTest(char *cmd, SFCmdS *value);
int asensorTest(char *cmd, SFCmdS *value);
int psensorTest(char *cmd, SFCmdS *value);
int lsensorTest(char *cmd, SFCmdS *value);
int msensorTest(char *cmd, SFCmdS *value);
int capsensorTest(char *cmd, SFCmdS *value);


int testFlashLight(const char* cmd);

int CustomerCmd(SFCmdS *in, SFCmdS* out);
int ModemCmd(SFCmdS *in, SFCmdS* out);

int NVCmd(SFCmdS *in, SFCmdS *out);

int NfcTest(char* cmd);

int setCurrent(const char* cmd, SFCmdS* value);
int getVoltage(const char* cmd, SFCmdS* value);

int ControlCharge(int enable);
int NvErase(SFCmdS* in, SFCmdS* out);

#ifdef __cplusplus
}
#endif
#endif

