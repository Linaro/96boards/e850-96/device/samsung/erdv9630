#ifndef __ENGMODE_DATA_H__
#define __ENGMODE_DATA_H__

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_FACTORY_CMD_PARAM_SIZE (1024*2)
#define MAX_FACTORY_EXTRA_INFO_SIZE (1024)


typedef struct factory_cmd {
	char param[MAX_FACTORY_CMD_PARAM_SIZE];
	int param_len;
	int revert1;
	int revert2;
	void* revert3;
} SFCmdS;

enum ErrCode {
	HAL_E_SUCCESS = 0,
	HAL_E_ERR = 1,               /* for normal error! */
	HAL_E_TIMEOUT = 2,           /* for timeout! */
	HAL_E_COMM_ERR = 3,           /* for communication error! */
	HAL_E_UNSUPPORT = 4,
	HAL_E_PARAM_ERR = 5,
	HAL_E_MAX
};

#define RESPONSE_OK	"OK"
#define RESPONSE_ERROR	"ERROR"



#ifdef __cplusplus
}
#endif
#endif
