#ifndef __ENGMODE_FUNCTIONS_H__
#define __ENGMODE_FUNCTIONS_H__

#include "data.h"
#ifdef __cplusplus
extern "C" {
#endif

int erase_operate_cmd(SFCmdS* in, SFCmdS* out);
int adb_operate_cmd(SFCmdS* in, SFCmdS* out);
int battery_test_cmd(SFCmdS* in, SFCmdS* out);
int backlight_test_cmd(SFCmdS* in, SFCmdS* out);
int gyro_operate_cmd(SFCmdS* in, SFCmdS* out);
int accelerometer_operate_cmd(SFCmdS* in, SFCmdS* out);
int lsensor_operate_cmd(SFCmdS* in, SFCmdS* out);
int psensor_operate_cmd(SFCmdS* in, SFCmdS* out);
int magnetometer_operate_cmd(SFCmdS* in, SFCmdS* out);
int capsensor_operate_cmd(SFCmdS* in, SFCmdS* out);

int audio_operate_cmd(SFCmdS* in, SFCmdS* out);
int sdcard_checkdata_cmd(SFCmdS* in, SFCmdS* out);
int memory_check_cmd(SFCmdS* in, SFCmdS* out);
int storage_checkdata_cmd(SFCmdS* in, SFCmdS* out);
int flashlight_operate_cmd(SFCmdS* in, SFCmdS* out);
int version_cmd(SFCmdS* in, SFCmdS* out);
int system_sleep_cmd(SFCmdS* in, SFCmdS* out);
int ChipId_get_cmd(SFCmdS* in, SFCmdS* out);
int reboot_operate_cmd(SFCmdS* in, SFCmdS* out);

int nvdata_operate_cmd(SFCmdS* in, SFCmdS* out);
int mpversion_operate_cmd(SFCmdS* in, SFCmdS* out);
int mpname_operate_cmd(SFCmdS* in, SFCmdS* out);
int hwid_operate_cmd(SFCmdS* in, SFCmdS* out);
int hwversion_operate_cmd(SFCmdS* in, SFCmdS* out);
int current_operate_cmd(SFCmdS* in, SFCmdS* out);
int temperature_operate_cmd(SFCmdS* in, SFCmdS* out);
int voltage_operate_cmd(SFCmdS* in, SFCmdS* out);
int nfc_operate_cmd(SFCmdS* in, SFCmdS* out);
int fastcharger_operate_cmd(SFCmdS* in, SFCmdS* out);

int customer_operate_cmd(SFCmdS* in, SFCmdS* out);
int modem_operate_cmd(SFCmdS* in, SFCmdS* out);
int nv_erase_cmd(SFCmdS* in, SFCmdS* out);
int efuse_operate_cmd(SFCmdS* in, SFCmdS* out);
int freq_check_cmd(SFCmdS* in, SFCmdS* out);
int cpu_check_cmd(SFCmdS* in, SFCmdS* out);
int rpmb_operate_cmd(SFCmdS* in, SFCmdS* out);
int pmic_check_cmd(SFCmdS* in, SFCmdS* out);
int provision_key_cmd(SFCmdS* in, SFCmdS* out);
int keybox_operate_cmd(SFCmdS* in, SFCmdS* out);
int key_operate_cmd(SFCmdS* in, SFCmdS* out);


#ifdef __cplusplus
}
#endif
#endif

