#include "vendor.h"
#include "data.h"

#include "backlight/backlight.h"
#include "nv/nvtest.h"
#include <hardware/sensors-base.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <cutils/sockets.h>
#include "modem/modem.h"

#define CONFIG_NUM 9

using android::base::GetProperty;
using android::base::SetProperty;

int closeAdb()
{
    SetProperty("persist.sys.usb.config", "none");
    SetProperty("persist.sys.usb.config", "rndis,acm,dm");
    std::string prop = GetProperty("init.svc.adbd", "");

    if(!strcmp(prop.c_str(), "running"))
    {
        return 0;
    }
    return 1;
}

int openAdb()
{
    SetProperty("persist.sys.usb.config", "none");
    SetProperty("persist.sys.usb.config", "dm,acm,adb");
    std::string prop = GetProperty("init.svc.adbd", "");

    if(!strcmp(prop.c_str(), "running"))
    {
        return 0;
    }

    return 1;
}

int getChipId(SFCmdS *value)
{
    return 0;
}

//0-halt reboot
//1-sfactory reboot
//2-normal reboot
//3-recovery reboot
//4-fastboot reboot

void reboot(int mode)
{
    switch(mode)
    {
        case 0:
            SetProperty("sys.powerctl", "shutdown");
            break;
        case 1:
            SetProperty("sys.powerctl", "reboot,sfactory");
            break;
        case 2:
            SetProperty("sys.powerctl", "reboot");
            break;
        case 3:
            SetProperty("sys.powerctl", "reboot,recovery");
            break;
        case 4:
            SetProperty("sys.powerctl", "reboot,bootloader");
            break;
        default:
            break;
    }
    return;
}

int getVersion(SFCmdS *value)
{
    //Android version
    std::string android_version = GetProperty("ro.build.description", "");
    //LOGD("%s, Android version:%s", __FUNCTION__, android_version);

    if(android_version.empty())
    {
        ALOGD("%s, Get Android version failure", __FUNCTION__);
        return 1;
    }

    //Kernel version
    int fd;
    char kernel_version[512];
    fd = open("/proc/version", O_RDONLY);
    read(fd, kernel_version, sizeof(kernel_version));
    close(fd);
    if(strcmp(kernel_version, "") == 0)
    {
        ALOGD("%s, Get Kernel version failure", __FUNCTION__);
        return 1;
    }

    char strResp[512];
    sprintf(strResp, "\r\nANDROID: %s\r\nKERNEL: %s", android_version.c_str(), kernel_version);
    //LOGD("%s, AP version:%s", __FUNCTION__, strResp);

    int len = strlen(strResp);
    while((strResp[len - 1] == '\n') || (strResp[len - 1] == '\r'))
    {
        strResp[len - 1] = '\0';
        len = strlen(strResp);
    }

    strcpy(value->param, strResp);
    value->param_len = len;
    return 0;
}

#include <linux/fb.h>

int systemSleep()
{
    int fd = -1;
    // turn off the lcd
    fd = open("/dev/graphics/fb0", O_RDONLY);
    if (fd < 0)
    {
        ALOGD("%s, open /dev/graphics/fb0 failed", __FUNCTION__);
        return -1;
    }
    int ret = ioctl(fd, FBIOBLANK, FB_BLANK_POWERDOWN);
    close(fd);
    if (ret < 0)
    {
        ALOGD("%s, call fb ioctl failed", __FUNCTION__);
        return -1;
    }

    // set the system into sleep status
    system("echo mem > /sys/power/autosleep");
    return 0;
}

int readBattery(SFCmdS *value)
{
    int fd;
    int len;
    const int SIZE = 128;
    char buf[SIZE] = {0X0,};

    fd = open("sys/class/power_supply/battery/capacity", O_RDONLY);
    read(fd, buf, SIZE);
    close(fd);

    len = strlen(buf);
    strcpy(value->param, buf);
    value->param_len = len;

    return 0;
}

int readAlarm(SFCmdS *value)
{
    return 1;
}

static int fileRead(const char *fname, char *buffer, int insize)
{
    int fd;
    int size;

    fd = open(fname, O_RDONLY);
    if (fd < 0)
    {
        ALOGE("%s: Failed to open %s", __FUNCTION__, fname);
        return 0;
    }

    size = read(fd, buffer, insize);
    close(fd);
    ALOGI("%s: %s: size:%d buffer %s", __FUNCTION__, fname, size, buffer);

    return size;
}

int getStorage(SFCmdS *value)
{
    char readbuf[32];
    int capa = 0;
    int size = fileRead("/sys/block/sda/size", readbuf, sizeof(readbuf));

    if (size > 0)
    {
        /* recalculation to MB */
        capa = atoi(readbuf);
        capa = capa / 2 / 1024;
        sprintf(readbuf, "%dM", capa);
        strncpy(value->param, readbuf, size);
        value->param_len = strlen(value->param);
        return 0;
    }

    return 1;
}

int getMemory(SFCmdS *value)
{
    char readbuf[32];
    unsigned long capa = 0;
    int size = fileRead("/sys/devices/system/chip-id/memsize", readbuf, sizeof(readbuf));

    if (size > 0)
    {
        capa = atol(readbuf);
        capa = capa / 1024 / 1024;
        sprintf(readbuf, "%dM", capa);
        strncpy(value->param, readbuf, size);
        value->param_len = strlen(value->param);
        return 0;
    }
    return 1;
}

#define TCMD_GET_CPVERSION "AT+SWVERSION?\r\n"
#define TCMD_GET_PA_TEMPERATURE "AT+TEMPERATURE=0\r\n"

#define SOCKET_SYNC_SMCD "/dev/socket/sync_smcd"
int SendATCommand(const uint8_t *cmd, int length, uint8_t *resp, int *resplen)
{
    int err = -1;
    int cnt = 0;
    fd_set fdset;

    int fd = socket_local_client(SOCKET_SYNC_SMCD, ANDROID_SOCKET_NAMESPACE_ABSTRACT, SOCK_STREAM);
    if(fd < 0)
    {
        ALOGE("%s : open socket(%s) fail(%d)\n", __func__, SOCKET_SYNC_SMCD, errno);
        return -1;
    }
    send(fd, (const void *)cmd, length, 0);
    //receive the at response
    while (1)
    {
        FD_ZERO(&fdset);
        FD_SET(fd, &fdset);
        struct timeval timeout = {15, 0};
        err = select(fd + 1, &fdset, NULL, NULL, &timeout);
        if (err < 0)
        {
            if (errno == EINTR)
            {
                continue;
            }
            ALOGE("%s : select err = %d\n", __func__, errno);
            break;
        }
        else if (err == 0)
        {
            ALOGW("%s:: timeout.\n", __func__);
            break;
        }

        if (FD_ISSET(fd, &fdset))
        {
            memset(resp, 0x0, 2048);
            cnt = read(fd, resp, 2048);
            if (cnt < 0)
            {
                if(errno == EINTR || errno == EAGAIN)
                {
                    continue;
                }
                ALOGE("%s : Fail to read %s, error = %d\n", __FUNCTION__, SOCKET_SYNC_SMCD, errno);
            }
            else if (cnt == 0)
            {
                ALOGE("%s : No data on %s\n", __FUNCTION__, SOCKET_SYNC_SMCD);
                break;
            }
            ALOGD("%s : recive size(%d)\n", __FUNCTION__, cnt);
            break;
        }
    }
    close(fd);
    *resplen = cnt;
    return 0;
}

int getCPVersion(SFCmdS *value)
{
    uint8_t *resp = new uint8_t[2048];
    int resplen = 0;
    const char *cpVersion = TCMD_GET_CPVERSION;
    int ret;

    ret = SendATCommand((const uint8_t *)cpVersion, strlen(cpVersion), resp, &resplen);

    size_t endPos = std::string::npos;
    size_t startPos = std::string::npos;
    std::string str;
    std::string cpStr;

    if(resplen)
    {
        str = (const char *)resp;
    }
    else
    {
        ALOGE("[%s] CP version size is 0", __FUNCTION__);
        delete[] resp;
        return 1;
    }

    if((endPos = str.find(AT_OK_RESPONSE)) != std::string::npos)
    {
        if((startPos = str.find(COLON_SYMBOL)) != std::string::npos)
        {
            ++startPos; //ignore colon symbol":"
            int len = endPos - startPos - 2;
            //OK response string format:AT+command\r\n+command:[result string]\r\n\r\nOK\r\n
            //FAIL response string format:AT+command\r\n\r\nERROR\r\n
            std::string cutstr = str.substr(startPos, len);
            ALOGD("[%s]::[%s]", __FUNCTION__, cutstr.c_str());

            /*cmd CP:version info\r\n*/
            cpStr = "\r\nCP:" + cutstr + "\r\n";
        }
        else
        {
            ret = 1;
        }
    }
    else
    {
        ret = 1;
    }

    if(!ret)
    {
        // Attach the CP version
        std::string mpVerStr = value->param;
        mpVerStr += cpStr;
        memcpy(value->param, mpVerStr.c_str(), mpVerStr.length());
        value->param_len = strlen(value->param);
    }
    delete[] resp;

    return ret;
}

int getMPVersion(SFCmdS *value)
{
    int ret;
#if 0
    // Get AP version
    ret = getVersion(value);
    if(ret)
    {
        ALOGD("%s, Get AP version failure", __FUNCTION__);
        return 1;
    }

    // Get CP version
    ret = getCPVersion(value);
    if(ret)
    {
        ALOGD("%s, Get CP version failure", __FUNCTION__);
        return 1;
    }
#endif
    //Android version
    std::string android_version = GetProperty("ro.build.description", "");
    //ALOGD("%s, Android version:%s", __FUNCTION__, android_version);

    if(android_version.empty())
    {
        ALOGD("%s, Get Android version failure", __FUNCTION__);
        return 1;
    }

    char strResp[512] = {0,};
    sprintf(strResp, "\r\nANDROID: %s", android_version.c_str());
    ALOGD("%s, AP version:%s", __FUNCTION__, strResp);

    int len = strlen(strResp);
    while((strResp[len - 1] == '\n') || (strResp[len - 1] == '\r'))
    {
        strResp[len - 1] = '\0';
        len = strlen(strResp);
    }

    strcpy(value->param, strResp);
    value->param_len = len;

    return 0;
}

int getMPName(SFCmdS *value)
{
    return 0;
}

int getSDCard(SFCmdS *value)
{
    char readbuf[32];
    int capa = 0;
    int size = fileRead("/sys/block/mmcblk0/size", readbuf, sizeof(readbuf));

    if (size > 0)
    {
        /* recalculation to MB */
        capa = atoi(readbuf);
        capa = capa / 2 / 1024;
        sprintf(readbuf, "%dM", capa);
        strncpy(value->param, readbuf, size);
        value->param_len = strlen(value->param);
        return 0;
    }

    return 1;
}

int getHWID(SFCmdS *value)
{
    return 1;
}


int getHWVersion(SFCmdS *value)
{
    return 1;
}

int getCPUTemp(SFCmdS *value)
{
    return 1;
}

int getChargerTemp(SFCmdS *value)
{
    return 1;
}

int getPATemp(SFCmdS *value)
{
    return 1;
}

int getBatteryTemp(SFCmdS *value)
{
    return 1;
}

int earsePartition(char *partition, SFCmdS *value)
{
    return 1;
}

int audioCmd(const char *cmd, SFCmdS *value)
{
    return 1;
}

int testBackLight(int value)
{
    char str[256];
    memset(str, 0, 255);
    sprintf(str, "echo %d > /sys/class/backlight/backlight/brightness", value);
    system(str);
    return 0;
}

int gyroTest(char *cmd, SFCmdS *value)
{
    return 1;
}

int asensorTest(char *cmd, SFCmdS *value)
{
    return 1;
}

int lsensorTest(char *cmd, SFCmdS *value)
{
    return 1;
}

int psensorTest(char *cmd, SFCmdS *value)
{
    return 1;
}

int msensorTest(char *cmd, SFCmdS *value)
{
    return 1;
}

int capsensorTest(char *cmd, SFCmdS *value)
{
    ALOGD ("%s, cmd::%s\n", __FUNCTION__, cmd);
    return 1;
}



int testFlashLight(const char *cmd)
{
    return 1;
}

#define PROP_RFS_DIRECTORY_PATH         "vendor.ril.exynos.nvpath"
static const char *gsignature = "Samsung_SIT_RIL";

#include <fstream>
#include <iostream>
#include <openssl/md5.h>
#include <unistd.h>
#include <errno.h>
bool copy(const char *src, const char *dest)
{
    using namespace std;
    ifstream in(src, ios::binary | ios::in);
    if (!in.is_open())
    {
        //cout << "error open file " << src << endl;
        ALOGE("%s, error open file  (%s)", __FUNCTION__, src);
        return false;
    }
    ofstream out(dest, ios::binary | ios::trunc | ios::out);
    if (!out.is_open())
    {
        ALOGE("%s, error open file  (%s)", __FUNCTION__, dest);
        in.close();
        return false;
    }
    if (src == dest)
    {
        ALOGE("%s,the src file can't be same with dst file", __FUNCTION__);
        in.close();
        out.close();
        return false;
    }
    char buf[2048];
    long long totalBytes = 0;
    while(in)
    {
        in.read(buf, 2048);
        out.write(buf, in.gcount());
        totalBytes += in.gcount();
    }
    in.close();
    out.close();
    return true;
}

bool Write(const char *fname, const char *data, int len)
{
    using namespace std;
    ofstream out(fname, ios::binary | ios::trunc | ios::out);
    if (!out.is_open())
    {
        ALOGE("%s, error open file  (%s)", __FUNCTION__, fname);
        return false;
    }
    out.write(data, len);
    out.close();
    return true;

}

int Read(const char *fname, char *data, int len)
{
    int fd = -1;
    int rd = 0;

    fd = open(fname, O_RDWR | O_SYNC);
    lseek(fd, 0, SEEK_SET);
    rd = read(fd, (void *)data, len);
    close(fd);
    return rd;
}

/* calculate MD5 checksum */
int CsumCalc(const char *fname, char *csum, const char *signature)
{
    ALOGI("%s", __FUNCTION__);
    using namespace std;
    ifstream in(fname, ios::binary | ios::in);
    if (!in.is_open())
    {
        //cout << "error open file " << src << endl;
        ALOGE("%s, error open file  (%s)", __FUNCTION__, fname);
        return -1;
    }

    MD5_CTX md5;
    MD5_Init(&md5);

    unsigned char digest[16] = {0};
    unsigned char buf[1024];
    int fd = -1;
    int len = 0;

    fd = open(fname, O_RDWR | O_SYNC);
    lseek(fd, 0, SEEK_SET);

    do
    {
        len = read(fd, (void *)buf, 1024);
        if (len <= 0)
        {
            //ALOGE("[%s] Failed to read file(%s)", __FUNCTION__, strerror(errno));
            close(fd);
            break;
        }
        MD5_Update(&md5, buf, len);
    }
    while(len > 0);

//    const char* signature = "Samsung_SIT_RIL";
    MD5_Update(&md5, signature, strlen(signature));
    MD5_Final(digest, &md5);

    // done
    for (int i = 0; i < 16; i++)
    {
        snprintf(csum + (i * 2), 3, "%02x", digest[i]);
    }

    return 0;
}

static const char *rfs_nv_file_names[] =
{
    "nv_normal.bin",
    "nv_protected.bin",
    "nv_normal.bin.mk5",
    "nv_protected.bin.mk5",
};

struct
{
    const char *satcmd;
    const char *atcmd;
} gpWBCmd[] =
{
    {"SAT+BT=1",                          "ATA+BT=1"},
    {"SAT+BT=0",                          "ATA+BT=0"},
    {"SAT+WIFI=1",                        "ATA+WIFI=1"},
    {"SAT+WIFI=0",                        "ATA+WIFI=0"},
    {nullptr,                             nullptr},
};

int GetWBCmdBySATCmd(const char *cmd, std::string &outcmd)
{
    const char *satcmd = nullptr;
    for (int i = 0; i < sizeof(gpWBCmd) / sizeof(gpWBCmd[0]) - 1; i++)
    {
        satcmd = gpWBCmd[i].satcmd;
        if(satcmd == nullptr)
        {
            ALOGD("%s::sat WB command is nullptr", __FUNCTION__);
            continue;
        }
        //ALOGD("%s::internal cmd(%s), original cmd(%s)", __FUNCTION__, satcmd, cmd);

        if(strncmp(satcmd, cmd, strlen(satcmd)) == 0)
        {
            outcmd = gpWBCmd[i].atcmd;
            ALOGD("%s:: get WB command (%s)", __FUNCTION__, outcmd.c_str());
            return 0;
        }
    }
    return -1;
}

#define DUT_BT      0
#define DUT_WLAN    1

int SendWBCommand(const char *cmd, int length, std::string &resp, int module = DUT_BT)
{
#define SOCKET_SYNC_BT "/dev/socket/sync_bt"
#define SOCKET_SYNC_WLAN "/dev/socket/sync_wlan"

    int err = -1;
    int cnt = 0;
    fd_set fdset;
    char tempRes[256];
    memset(tempRes, 0, 256);
    int fd = -1;
    resp = "";
    const char *name = nullptr;
    if(module == DUT_BT)
    {
        name = SOCKET_SYNC_BT;
    }
    else if(module == 1)
    {
        name = SOCKET_SYNC_WLAN;
    }
    fd = socket_local_client(name, ANDROID_SOCKET_NAMESPACE_ABSTRACT, SOCK_STREAM);
    if(fd < 0)
    {
        ALOGE("%s : open socket(%s) fail(%d)\n", __func__, name, errno);
        return -1;
    }

    send(fd, cmd, length, 0);
    //receive the at response
    while (1)
    {
        FD_ZERO(&fdset);
        FD_SET(fd, &fdset);
        struct timeval timeout = {15, 0};
        err = select(fd + 1, &fdset, nullptr, nullptr, &timeout);
        if (err < 0)
        {
            if (errno == EINTR)
            {
                continue;
            }
            ALOGE("%s : select err = %d\n", __func__, errno);
            break;
        }
        else if (err == 0)
        {
            ALOGW("%s:: timeout.\n", __func__);
            break;
        }

        if (FD_ISSET(fd, &fdset))
        {
            memset(tempRes, 0, 256);
            cnt = read(fd, tempRes, 256);
            if (cnt < 0)
            {
                if(errno == EINTR || errno == EAGAIN)
                {
                    continue;
                }
                ALOGE("%s : Fail to read %s, error = %d\n", __FUNCTION__, name, errno);
            }
            else if (cnt == 0)
            {
                ALOGE("%s : No data on %s\n", __FUNCTION__, name);
                break;
            }
            ALOGD("%s : recive size(%d)\n", __FUNCTION__, cnt);
            break;
        }
    }
    close(fd);
    if(cnt > 0)
    {
        resp = tempRes;
    }
    return 0;
}

int CustomerCmd(SFCmdS *in, SFCmdS *out)
{
    if (!in || !out)
    {
        ALOGD("%s:: invalid in or out.", __FUNCTION__);
        return -1;
    }
    int ret = -1;

    ALOGD("%s:: in->param=%s, in->param_len=%d.", __FUNCTION__, in->param, in->param_len);
    if((strcmp(in->param, "SAT+BT=1") == 0)
       || (strcmp(in->param, "SAT+BT=0") == 0)
       || (strcmp(in->param, "SAT+WIFI=1") == 0)
       || (strcmp(in->param, "SAT+WIFI=0") == 0))
    {
        std::string res;
        //open wlan/bt factory mode
        SendWBCommand("RF+AP2WB", strlen("RF+AP2WB"), res);

        //send wlan/bt command
        std::string realcmd;
        ret = GetWBCmdBySATCmd((const char *)in->param, realcmd);
        if(ret != 0)
        {
            ALOGE("%s : no mapped wlan/bt command", __func__);
            return -1;
        }
        if((strcmp(in->param, "SAT+WIFI=1") == 0)
           || (strcmp(in->param, "SAT+WIFI=0") == 0))
        {
            //test wlan
            SendWBCommand(realcmd.c_str(), realcmd.length(), res, DUT_WLAN);
        }
        else
        {
            //test bt
            SendWBCommand(realcmd.c_str(), realcmd.length(), res, DUT_BT);
        }
        if(strncmp("OK", res.c_str(), strlen("OK")) == 0)
        {
            ret = 0;
        }
        //close factory mode
        SendWBCommand("RF+WB2AP", strlen("RF+WB2AP"), res);
        return ret;
    }

    return 1;
}

int ModemCmd(SFCmdS *in, SFCmdS *out)
{
    int ret = HAL_E_ERR;
    if(strlen(in->param) == 0 || in->param_len == 0)
    {
        return HAL_E_PARAM_ERR;
    }

    size_t endPos = string::npos;
    size_t startPos = string::npos;
    uint8_t *resp = nullptr;
    int resplen = 0;
    string str;

    int cmdLen = in->param_len + 2;
    uint8_t *pCmd = new uint8_t[cmdLen + 1];
    memset(pCmd, 0, cmdLen + 1);
    memcpy(pCmd, in->param, in->param_len);
    memcpy(pCmd + in->param_len, AT_CMD_ENG_SYMBOL, strlen(AT_CMD_ENG_SYMBOL));

    ret = SendSyncATCommand((const uint8_t *)pCmd, cmdLen, &resp, &resplen);
    if((ret != 0) || (resp == nullptr) || (resplen == 0))
    {
        ALOGE("%s:: at command response error", __FUNCTION__);
        delete[] pCmd;
        return ret;
    }
    ALOGD("%s:: at command response: %s", __FUNCTION__, resp);
    str = (const char *)resp;
    if((endPos = str.find(AT_OK_RESPONSE)) != string::npos)
    {
        if((startPos = str.find(COLON_SYMBOL)) != string::npos)
        {
            ++startPos; //ignore colon symbol":"
            int len = endPos - startPos - 2;
            string cutstr = str.substr(startPos, len);
            ALOGD("[%s]::[%s]", __FUNCTION__, cutstr.c_str());
            ret = CheckResultByResp(cutstr);
            memcpy(out->param, cutstr.c_str(), cutstr.length());
            out->param_len = cutstr.length();
        }
    }
    else
    {
        ret = HAL_E_COMM_ERR;
    }

    delete[] pCmd;
    return ret;
}


int NVCmd(SFCmdS *in, SFCmdS *out)
{
    int ret = 1;
    if(in == nullptr || in->param_len <= 0)
    {
        return 1;
    }
    //operate type: read/write command
    int opt = 0;
    char *p = nullptr;
    NVItem item;
    memset(&item, 0, sizeof(NVItem));
    p = strtok(in->param, ",");
    if(p)
    {
        ALOGD("%s, nv slot:%s\n", __FUNCTION__, p);
        item.slot = atoi(p);
    }
    p = strtok(nullptr, ",");
    if(p)
    {
        ALOGD("%s, nv offset:%s\n", __FUNCTION__, p);
        item.offset = atoi(p);
    }
    p = strtok(nullptr, ",");
    if(p)
    {
        ALOGD("%s, nv length:%s\n", __FUNCTION__, p);
        item.size = atoi(p);
    }
    p = strtok(nullptr, ",");
    if(p)
    {
        ALOGD("%s, nv data:%s\n", __FUNCTION__, p);
        memcpy(item.data, p, item.size);
        opt = 1;
    }
    NVFactoryTest *pnv =  NVFactoryTest::Instance();
    //opt == 0, read nv
    //opt == 1, write nv
    ret = pnv->RDWRNVItem(opt, item, OPERATE_TYPE_FILE);
    NVFactoryTest::Exitance();
    pnv = nullptr;
    if((ret == 0) && (opt == 0))
    {
        out->param_len = item.size;
        memcpy(out->param, item.data, item.size);
    }
    return ret;
}

int NfcTest(char *cmd)
{
    return 1;
}

int setCurrent(const char *cmd, SFCmdS *value)
{

    char str[256];
    memset(str, 0, 255);

    if(strcmp(cmd, "1") == 0)
    {
        // vBat charging
        sprintf(str, "chmod 777 /sys/class/power_supply/battery/calibrate");
        system(str);
        sprintf(str, "echo 0 > /sys/class/power_supply/battery/calibrate");
        system(str);
    }
    else
    {
        // vBus charging
        sprintf(str, "chmod 777 /sys/class/power_supply/battery/calibrate");
        system(str);
        sprintf(str, "echo 1 > /sys/class/power_supply/battery/calibrate");
        system(str);
    }

    return 0;
}

int getVoltage(const char *cmd, SFCmdS *value)
{

    int fd;
    int len;
    const int SIZE = 128;
    char buf[SIZE] = {0X0,};

    if(strcmp(cmd, "1") == 0)
    {
        fd = open("sys/class/power_supply/battery/voltage_now", O_RDONLY);
        read(fd, buf, SIZE);
        close(fd);

        len = strlen(buf);
        strcpy(value->param, buf);
        value->param_len = len;

    }
    else
    {
        fd = open("sys/class/power_supply/battery/input_voltage_regulation", O_RDONLY);
        read(fd, buf, SIZE);
        close(fd);

        len = strlen(buf);
        strcpy(value->param, buf);
        value->param_len = len;
    }

    return 0;
}

int ControlCharge(int enable)
{

    char str[256];
    memset(str, 0, 255);

    if(enable == 0)
    {
        sprintf(str, "echo 0 > /sys/class/power_supply/battery/calibrate");
    }
    else
    {
        sprintf(str, "echo 2 > /sys/class/power_supply/battery/calibrate");
    }

    system(str);

    return 0;
}

int NvErase(SFCmdS *in, SFCmdS *out)
{
    return HAL_E_ERR;
}


