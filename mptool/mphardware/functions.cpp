#include <android/log.h>
#include <string.h>

#include "functions.h"
#include "vendor.h"

int erase_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    return earsePartition(in->param, out);
}

int adb_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = 1;
    if(strcmp((const char *)in->param, "0") == 0)
    {
        ret = closeAdb();
    }
    else if(strcmp(in->param, "1") == 0)
    {
        ret = openAdb();
    }
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int battery_test_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = 1;
    if(strcmp(in->param, "0") == 0)
    {
        //Add stopCharging for RF test
        ret = ControlCharge(0);
    }
    else if(strcmp(in->param, "1") == 0)
    {
        //Add startCharging for RF test
        ret = ControlCharge(1);
    }
    else if(strcmp(in->param, "") == 0)
    {
        ret = readBattery(out);
    }
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int backlight_test_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = testBackLight(atoi(in->param));
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int gyro_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = gyroTest(in->param, out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int accelerometer_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = asensorTest(in->param, out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int lsensor_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = lsensorTest(in->param, out); // Read L-sensor value
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int psensor_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = psensorTest(in->param, out); // 2: Read value
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int magnetometer_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = msensorTest(in->param, out); // values of M-sensor got from the board
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int capsensor_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = capsensorTest(in->param, out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int audio_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = audioCmd(in->param, out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int sdcard_checkdata_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = getSDCard(out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int memory_check_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = getMemory(out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int storage_checkdata_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = getStorage(out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int flashlight_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = testFlashLight(in->param);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int version_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = getVersion(out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int hwid_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = getHWID(out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int hwversion_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = getHWVersion(out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int system_sleep_cmd(SFCmdS *in, SFCmdS *out)
{
    return systemSleep();
}

int reboot_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    if(strcmp(in->param, "0") == 0)
    {
        ALOGD("%s:: SAT+REBOOT=%s, ==> Power off!!!", __FUNCTION__, in->param);
        reboot(REBOOT_HALT);
    }
    else if(strcmp(in->param, "1") == 0)
    {
        ALOGD("%s:: SAT+REBOOT=%s, ==> Reboot to factory!!!", __FUNCTION__, in->param);
        reboot(REBOOT_FACTORY);
    }
    else if(strcmp(in->param, "2") == 0)
    {
        ALOGD("%s:: SAT+REBOOT=%s, ==> Reboot to android!!!", __FUNCTION__, in->param);
        reboot(REBOOT_NORMAL);
    }
    else if(strcmp(in->param, "3") == 0)
    {
        ALOGD("%s:: SAT+REBOOT=%s, ==> Reboot to recovery!!!", __FUNCTION__, in->param);
        reboot(REBOOT_RECOVERY);
    }
    else if(strcmp(in->param, "4") == 0)
    {
        ALOGD("%s:: SAT+REBOOT=%s, ==> Reboot to fastboot!!!", __FUNCTION__, in->param);
        reboot(REBOOT_FASTBOOT);
    }
    else
    {
        ALOGE("%s:: error command(%s)", __FUNCTION__, in->param);
    }
    return 0;
}

int nvdata_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    return NVCmd(in, out);
}

int mpversion_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = getMPVersion(out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int mpname_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = getMPName(out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int current_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = setCurrent(in->param, out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int temperature_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = 1;

    if(strcmp(in->param, "0") == 0)
    {
        ret = getCPUTemp(out);
    }
    else if(strcmp(in->param, "1") == 0)
    {
        ret = getChargerTemp(out);
    }
    else if(strcmp(in->param, "2") == 0)
    {
        ret = getPATemp(out);
    }
    else if(strcmp(in->param, "3") == 0)
    {
        ret = getBatteryTemp(out);
    }

    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int voltage_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = getVoltage(in->param, out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int nfc_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = 1;

    if (in == nullptr)
    {
        return ret;
    }
    ret = NfcTest(in->param);

    return ret;
}

int fastcharger_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    return 1;
}

int customer_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = CustomerCmd(in, out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int modem_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    int ret = ModemCmd(in, out);
    ALOGD("%s:: result value(%d)", __FUNCTION__, ret);
    return ret;
}

int nv_erase_cmd(SFCmdS *in, SFCmdS *out)
{
    return NvErase(in, out);
}
int efuse_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    return HAL_E_ERR;
}
int freq_check_cmd(SFCmdS *in, SFCmdS *out)
{
    return HAL_E_ERR;
}
int cpu_check_cmd(SFCmdS *in, SFCmdS *out)
{
    return HAL_E_ERR;
}
int rpmb_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    return HAL_E_ERR;
}
int pmic_check_cmd(SFCmdS *in, SFCmdS *out)
{
    return HAL_E_ERR;
}
int provision_key_cmd(SFCmdS *in, SFCmdS *out)
{
    return HAL_E_ERR;
}
int keybox_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    return HAL_E_ERR;
}
int key_operate_cmd(SFCmdS *in, SFCmdS *out)
{
    return HAL_E_ERR;
}


