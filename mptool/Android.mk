#include $(call all-subdir-makefiles)
ifneq (,$(filter pcba, $(BOARD_USE_SSCR_TOOLS)))
dir += \
	mphardware
endif

ifneq (,$(filter wcn, $(BOARD_USE_SSCR_TOOLS)))
dir += \
	wlbt
endif

ifneq (,$(filter pcba modem wcn console fm, $(BOARD_USE_SSCR_TOOLS)))
dir += \
	json
endif

include $(call all-named-subdir-makefiles,$(dir))
