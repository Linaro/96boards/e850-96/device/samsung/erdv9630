# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for erd980 hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps). Except for a few implementation
# details, it only fundamentally contains two inherit-product
# lines, full and erd980, hence its name.
#

ifeq ($(TARGET_PRODUCT),full_erdv9630_pqr)
# Live Wallpapers
PRODUCT_PACKAGES += \
        LiveWallpapers \
        LiveWallpapersPicker \
        MagicSmokeWallpapers \
        VisualizationWallpapers \
        librs_jni

PRODUCT_PROPERTY_OVERRIDES := \
        net.dns1=8.8.8.8 \
        net.dns2=8.8.4.4

# Inherit from those products. Most specific first.
$(call inherit-product, device/samsung/erdv9630/device.mk)
# Common Telephony features
#$(call inherit-product, vendor/samsung_slsi/telephony/common/device-vendor.mk)

#
# ADDITIONAL VENDOR BUILD PROPERTIES (Telephony)
#
PRODUCT_PROPERTY_OVERRIDES += \
    persist.radio.multisim.config=dsds

PRODUCT_PROPERTY_OVERRIDES += \
    ro.logd.size=2M \
    radio.smsdomain=0 \
    telephony.lteOnCdmaDevice=1

PRODUCT_PROPERTY_OVERRIDES += \
    ro.debug_level=0x494d \

TARGET_SOC := exynos980
TARGET_SOC_BASE := exynos9630
TARGET_BOARD_PLATFORM := erdv980
TARGET_BOOTLOADER_BOARD_NAME := exynos980

PRODUCT_NAME := full_erdv9630_pqr
PRODUCT_DEVICE := erdv9630
PRODUCT_BRAND := Android
PRODUCT_MODEL := Full Android on ERD980
PRODUCT_MANUFACTURER := Samsung Electronics Co., Ltd.
TARGET_LINUX_KERNEL_VERSION := 4.9

PRODUCT_PACKAGES += ShannonIms

# AB_ENABLE := false

endif
