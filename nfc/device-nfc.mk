#############################################################################################
## Includes NFC Packages
#############################################################################################

# Modify it to project environment
LOCAL_PATH                     := device/samsung/erdv9630
BOARD_USES_SAMSUNG_NFC_CHIP    := rn82

$(warning "Building S.LSI SEC NFC packages...")
# HAL
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/nfc/init.nfc.sec.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.nfc.sec.rc \
    $(LOCAL_PATH)/nfc/bin/sec_s3nrn82_firmware.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/sec_s3nrn82_firmware.bin \
    $(LOCAL_PATH)/nfc/bin/sec_s3nrn82_rfreg.bin:$(TARGET_COPY_OUT_VENDOR)/etc/sec_s3nrn82_rfreg.bin

# Feature files + configuration
ifneq ($(TARGET_BUILD_VARIANT),user)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/nfc/configs/libnfc-nci_debug.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-nci.conf \
    $(LOCAL_PATH)/nfc/configs/libnfc-sec-vendor_debug_$(BOARD_USES_SAMSUNG_NFC_CHIP).conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-sec-vendor.conf
else
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/nfc/configs/libnfc-nci.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-nci.conf \
    $(LOCAL_PATH)/nfc/configs/libnfc-sec-vendor_$(BOARD_USES_SAMSUNG_NFC_CHIP).conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-sec-vendor.conf
endif

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.nfc.xml:system/etc/permissions/android.hardware.nfc.xml \
    frameworks/native/data/etc/android.hardware.nfc.hce.xml:system/etc/permissions/android.hardware.nfc.hce.xml \
    frameworks/native/data/etc/android.hardware.nfc.hcef.xml:system/etc/permissions/android.hardware.nfc.hcef.xml \
    $(LOCAL_PATH)/nfc/android.software.nfc.beam.xml:system/etc/permissions/android.software.nfc.beam.xml

# HIDL service
PRODUCT_PACKAGES += \
    nfc_nci_sec \
    android.hardware.nfc@1.2-service.sec \
    sec_nfc_test

# NFC stack and app service
PRODUCT_PACKAGES += \
    libnfc-sec \
    libnfc_sec_jni \
    NfcSec \
    Tag \
#    com.android.nfc_extras
