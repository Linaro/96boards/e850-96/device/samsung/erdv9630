ifeq ($(BOARD_USES_EXYNOS_GNSS_HAL), true)

#  only GPS libraries and binaries to the target directory
GPS_ROOT := device/samsung/erdv9630/gnss_binaries/release

PRODUCT_COPY_FILES += \
    $(GPS_ROOT)/gnssd:vendor/bin/hw/gpsd \
    $(GPS_ROOT)/lib_gnss.so:vendor/lib64/hw/gps.default.so \
    $(GPS_ROOT)/ca.pem:vendor/etc/gnss/ca.pem \
    $(GPS_ROOT)/android.hardware.location.gps.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.location.gps.xml \
    $(GPS_ROOT)/vendor.samsung.hardware.gnss@1.0-impl.so:vendor/lib64/hw/vendor.samsung.hardware.gnss@1.0-impl.so \
    $(GPS_ROOT)/android.hardware.gnss@2.1-impl.so:vendor/lib64/hw/android.hardware.gnss@2.1-impl.so

# use network ports (only for userdebug and eng builds)
ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_COPY_FILES += \
    $(GPS_ROOT)/gps.cfg.debug:vendor/etc/gnss/gps.cfg
else
PRODUCT_COPY_FILES += \
    $(GPS_ROOT)/gps.cfg:vendor/etc/gnss/gps.cfg
endif

endif
