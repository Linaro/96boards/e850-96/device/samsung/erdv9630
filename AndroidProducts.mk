#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/aosp_erd9630.mk \
    $(LOCAL_DIR)/full_smdk9630.mk \
    $(LOCAL_DIR)/full_erdv9630_qr.mk \
    $(LOCAL_DIR)/full_erdv9630_qrs.mk \
    $(LOCAL_DIR)/full_erdv9630_pqr.mk

COMMON_LUNCH_CHOICES := \
	full_erdv9630_qr-eng \
	full_erdv9630_qr-userdebug \
	full_erdv9630_qrs-eng \
	full_erdv9630_qrs-userdebug \
	full_erdv9630_pqr-eng \
	full_erdv9630_pqr-userdebug
