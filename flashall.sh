
fastboot flash bootloader out/target/product/erdv9630/bootloader.img
fastboot flash dtbo out/target/product/erdv9630/dtbo.img
fastboot flash boot out/target/product/erdv9630/boot.img
fastboot flash vbmeta out/target/product/erdv9630/vbmeta.img
fastboot flash recovery out/target/product/erdv9630/recovery.img

fastboot reboot fastboot

fastboot flash vendor out/target/product/erdv9630/vendor.img
fastboot flash system out/target/product/erdv9630/system.img

fastboot reboot
fastboot -w
