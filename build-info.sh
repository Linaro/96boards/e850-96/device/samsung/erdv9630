#!/bin/bash

PRODUCT_BOARD=erd9630

# build information for kernel
KERNEL_CROSS_COMPILE_PATH="$ROOT_DIR/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-4.9/bin/aarch64-linux-android-"
KERNEL_DEFCONFIG="erd9630_defconfig"
KERNEL_DTB="exynos9630-erd9630.dtb"
KERNEL_ARCH="arm64"
KERNEL_DIR="$ROOT_DIR/kernel/exynos"

# build information for bl2
#BL2_CROSS_COMPILE_PATH="$ROOT_DIR/bootable/misc_bin/exynos_prebuilts/aarch64-linux-android-4.9-2014/bin/aarch64-linux-android-"
BL2_CROSS_COMPILE_PATH=$CROSS_COMPILE
BL2_IMG="ERD9630_bl2.bin"
BL2_DEFCONFIG="erd9630_aarch64"
BL2_DIR="$ROOT_DIR/bootable/bl2-samsung-dev/"

# build information for uboot
#UBOOT_CROSS_COMPILE_PATH="$ROOT_DIR/bootable/misc_bin/exynos_prebuilts/aarch64-linux-android-4.9-2014/bin/aarch64-linux-android-"
UBOOT_CROSS_COMPILE_PATH=$CROSS_COMPILE
UBOOT_IMG="u-boot.bin"
UBOOT_DEFCONFIG="erd9630_aarch64"
UBOOT_ARCH="arm"
UBOOT_DIR="$ROOT_DIR/bootable/u-boot-samsung-201207"

# el3mon
EL3MON_CROSS_COMPILE_PATH="$CROSS_COMPILE64"
EL3MON_IMG="el3_mon.bin"
EL3MON_EPBL_IMG="epbl.bin"
EL3MON_EPBL_RAW_IMG="epbl_pad.bin"
EL3MON_DIR="$ROOT_DIR/bootable/el3_mon-samsung-dev"
EL3MON_DEFCONFIG="exynos9630_aarch64_config"

# build information for miscellaneous binary
BIN_DIR="$ROOT_DIR/bootable/misc_bin/erdv9630"
