#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifneq ($(filter full_erdv9630_qr full_erdv9630_qrs, $(TARGET_PRODUCT)),)
PRODUCT_SHIPPING_API_LEVEL := 29
else
PRODUCT_SHIPPING_API_LEVEL := 28
endif

include $(LOCAL_PATH)/BoardConfig.mk

BOARD_PREBUILTS := device/samsung/$(TARGET_PRODUCT:full_%=%)-prebuilts

INSTALLED_KERNEL_TARGET := $(BOARD_PREBUILTS)/kernel
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILTS)/dtb.img
BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
BOARD_PREBUILT_BOOTLOADER_IMG := $(BOARD_PREBUILTS)/bootloader.img
PRODUCT_COPY_FILES += $(foreach image,\
		      $(filter-out $(BOARD_PREBUILT_DTBOIMAGE) $(BOARD_PREBUILT_BOOTLOADER_IMG), $(wildcard $(BOARD_PREBUILTS)/*)),\
		      $(image):$(PRODUCT_OUT)/$(notdir $(image)))

# for tunnel
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.ipsec_tunnels.xml:vendor/etc/permissions/android.software.ipsec_tunnels.xml

ifneq ($(filter full_erdv9630_qr full_erdv9630_qrs, $(TARGET_PRODUCT)),)
# Enable Dynamic Partitions
PRODUCT_USE_DYNAMIC_PARTITIONS := true
else
BOARD_BUILD_SYSTEM_ROOT_IMAGE := true
endif

# System AS Root & Dynamic Partition support handle
ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS), true)
# Dynamic Partitions options & config
ifeq ($(AB_OTA_UPDATER), true)
# Super partition for A/B Device
BOARD_SUPER_PARTITION_SIZE := 7549747200
else
BOARD_SUPER_PARTITION_SIZE := 3774873600
endif
BOARD_SUPER_PARTITION_GROUPS := group_basic
BOARD_GROUP_BASIC_SIZE := 3770679296
BOARD_GROUP_BASIC_PARTITION_LIST := system vendor
endif

# For A/B device
ifeq ($(AB_OTA_UPDATER),true)
AB_OTA_PARTITIONS := \
  vbmeta \
  boot \
  system \
  dtbo \
  vendor
BOARD_USES_RECOVERY_AS_BOOT := true
PRODUCT_PACKAGES += \
  android.hardware.boot@1.0-impl \
  android.hardware.boot@1.0-impl.recovery \
  android.hardware.boot@1.0-service \
  bootctrl.$(TARGET_BOOTLOADER_BOARD_NAME) \
  bootctrl.$(TARGET_BOOTLOADER_BOARD_NAME).recovery \
  bootctl \
  update_engine \
  update_verifier
endif

# recovery mode
ifneq ($(filter full_erdv9630_qr full_erdv9630_qrs, $(TARGET_PRODUCT)),)
BOARD_INCLUDE_RECOVERY_DTBO := true
BOARD_INCLUDE_DTB_IN_BOOTIMG := true
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_BOOTIMG_HEADER_VERSION := 2
ifeq ($(AB_OTA_UPDATER),true)
BOARD_MKBOOTIMG_ARGS := \
   --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
   --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
   --header_version $(BOARD_BOOTIMG_HEADER_VERSION) \
   --recovery_dtbo $(BOARD_PREBUILT_DTBOIMAGE) \
   --dtb $(INSTALLED_DTBIMAGE_TARGET) \
   --dtb_offset 0
else
BOARD_MKBOOTIMG_ARGS := \
   --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
   --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
   --header_version $(BOARD_BOOTIMG_HEADER_VERSION) \
   --dtb $(INSTALLED_DTBIMAGE_TARGET) \
   --dtb_offset 0
endif
else
BOARD_INCLUDE_RECOVERY_DTBO := true
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_BOOTIMG_HEADER_VERSION := 1
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOTIMG_HEADER_VERSION) \
  --second $(INSTALLED_DTBIMAGE_TARGET) \
  --second_offset 0
endif

# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
    persist.demo.hdmirotationlock=false \
    dev.usbsetting.embedded=on \
    ro.vendor.config.release_version=$(TARGET_SOC)_QP1A_AP_200319

ifneq ($(filter full_erdv9630_qr, $(TARGET_PRODUCT)),)
# Device Manifest, Device Compatibility Matrix for Treble
ifeq ($(AB_OTA_UPDATER), true)
DEVICE_MANIFEST_FILE := \
    device/samsung/erdv9630/manifest_qr_ab.xml
else
DEVICE_MANIFEST_FILE := \
    device/samsung/erdv9630/manifest_qr.xml
endif
else
ifneq ($(filter full_erdv9630_qrs, $(TARGET_PRODUCT)),)
DEVICE_MANIFEST_FILE := \
    device/samsung/erdv9630/manifest_qrs.xml
else
# Device Manifest, Device Compatibility Matrix for Treble
DEVICE_MANIFEST_FILE := \
    device/samsung/erdv9630/manifest_pqr.xml
endif
endif

DEVICE_MATRIX_FILE := \
    device/samsung/erdv9630/compatibility_matrix.xml

# These are for the multi-storage mount.
ifeq ($(BOARD_USES_SDMMC_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/erdv9630/overlay-sdboot
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/erdv9630/overlay-ufsboot
else
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/erdv9630/overlay-emmcboot
endif
endif
DEVICE_PACKAGE_OVERLAYS += device/samsung/erdv9630/overlay

# exynosdisplayfeature_client
PRODUCT_PACKAGES += \
	exynosdisplayfeature_client \
	libhdrInterface \
	libdqeInterface

# Init files
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/$(CONF_NAME)/init.exynos9630.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).rc \
	device/samsung/erdv9630/$(CONF_NAME)/init.exynos9630.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).usb.rc \
	device/samsung/erdv9630/$(CONF_NAME)/ueventd.exynos9630.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc \
	device/samsung/erdv9630/$(CONF_NAME)/init.recovery.exynos9630.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.recovery.$(TARGET_SOC).rc \
	device/samsung/erdv9630/$(CONF_NAME)/init.recovery.exynos9630.rc:root/init.recovery.$(TARGET_SOC).rc

# for off charging mode
PRODUCT_PACKAGES += \
	charger_res_images

ifeq ($(BOARD_USES_SDMMC_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630.sdboot:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
ifeq ($(AB_OTA_UPDATER), true)
# UFS, Dynamic Partition, A/B Device
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630.dp.ab:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC) \
	device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630.dp.ab:$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC) \
	device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630.dp.ab:$(TARGET_COPY_OUT_RECOVERY)/root/first_stage_ramdisk/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630.dp.ab
else
# UFS, Dynamic Partition
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630.dp:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC) \
	device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630.dp:$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630.dp
endif
else
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630
endif
else
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/$(CONF_NAME)/fstab.exynos9630.emmc:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
endif
endif

# Support devtools
PRODUCT_PACKAGES += \
	Development

# Filesystem management tools
PRODUCT_PACKAGES += \
	e2fsck

# RPMB TA and test application
PRODUCT_PACKAGES += \
	tlrpmb \
	tlrpmb_64

# RPMB test application (only for eng build)
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
	bRPMB \
	bRPMB-CMD
endif

# CBD (CP booting deamon)
CBD_USE_V2 := true
CBD_PROTOCOL_SIT := true
CBD_DUMP_LIMIT := true

BOARD_USE_FM_RADIO := true
# FM Test App
PRODUCT_PACKAGES += \
	FMRadioService \
	privapp-permissions-fmradio.xml

# boot animation files
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/bootanim/bootanimation_part1.zip:system/media/bootanimation.zip \
    device/samsung/erdv9630/bootanim/shutdownanimation.zip:system/media/shutdownanimation.zip

# color mode
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    persist.sys.sf.color_saturation=1.0

# dqe calib xml
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/dqe/calib_data_bypass.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_bypass.xml \
    device/samsung/erdv9630/dqe/calib_data_colortemp.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_colortemp.xml \
    device/samsung/erdv9630/dqe/calib_data_eyetemp.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_eyetemp.xml \
    device/samsung/erdv9630/dqe/calib_data_rgbgain.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_rgbgain.xml \
    device/samsung/erdv9630/dqe/calib_data_rgbgain_limit_1_5.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_rgbgain_limit_1_5.xml \
    device/samsung/erdv9630/dqe/calib_data_rgbgain_limit_3_0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_rgbgain_limit_3_0.xml \
    device/samsung/erdv9630/dqe/calib_data_skincolor.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_skincolor.xml \
    device/samsung/erdv9630/dqe/calib_data_ce.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_ce.xml \
    device/samsung/erdv9630/dqe/calib_data_whitepoint.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_whitepoint.xml \
    device/samsung/erdv9630/dqe/calib_data.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data.xml \
    device/samsung/erdv9630/dqe/HDR_calib_data.png:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_default_img.png \
    device/samsung/erdv9630/dqe/HDR_calib_data.ppm:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_default_img.ppm \
    device/samsung/erdv9630/dqe/HDR_calib_lut_data.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_default_lut.xml \
    device/samsung/erdv9630/dqe/HDR_calib_aps_data.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_default_aps.xml \
    device/samsung/erdv9630/dqe/HDR_calib_cgc_data.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_default_cgc.xml \
    device/samsung/erdv9630/dqe/HDR_calib_hsc_data.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_default_hsc.xml \
    device/samsung/erdv9630/dqe/HDR_calib_gamma_data.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_default_gamma.xml \
    device/samsung/erdv9630/dqe/HDR_coef_data_default.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_coef_data_default.xml \
    device/samsung/erdv9630/dqe/DQE_coef_data_default.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/DQE_coef_data_default.xml

ifeq ($(BOARD_USES_EXYNOS_SENSORS_HAL),true)
# Enable support for chinook sensorhu
TARGET_USES_CHINOOK_SENSORHUB := false

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.gyroscope.xml\
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.light.xml\
    frameworks/native/data/etc/android.hardware.sensor.barometer.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.stepdetector.xml \
    device/samsung/erdv9630/init.exynos.nanohub.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.exynos.sensorhub.rc
#    device/samsung/erdv9630/firmware/chub_bl.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.bin \
    device/samsung/erdv9630/firmware/chub_nanohub_0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_0.bin \
    device/samsung/erdv9630/firmware/chub_nanohub_1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_1.bin \

# Copy CHUB os binary file
ifneq ("$(wildcard device/samsung/erdv9630/firmware/os.checked*.bin)", "")
PRODUCT_COPY_FILES += $(foreach image,\
    $(wildcard device/samsung/erdv9630/firmware/os.checked*.bin),\
    $(image):$(TARGET_COPY_OUT_VENDOR)/firmware/$(notdir $(image)))
else
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/chub_nanohub_0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_0.bin \
    device/samsung/erdv9630/firmware/chub_nanohub_1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_1.bin
endif

# Copy CHUB bl binary file
ifneq ("$(wildcard device/samsung/erdv9630/firmware/bl.unchecked.bin)", "")
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/bl.unchecked.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.bin
else
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/chub_bl.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.bin
endif

# Copy CHUB os elf file
ifneq ("$(wildcard device/samsung/erdv9630/firmware/os.checked*.elf)", "")
PRODUCT_COPY_FILES += $(foreach image,\
    $(wildcard device/samsung/erdv9630/firmware/os.checked*.elf),\
    $(image):$(TARGET_COPY_OUT_VENDOR)/firmware/$(notdir $(image)))
endif

# Copy CHUB bl elf file
ifneq ("$(wildcard device/samsung/erdv9630/firmware/bl.unchecked.elf)", "")
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/bl.unchecked.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.elf
endif

# Sensor HAL
#TARGET_USES_NANOHUB_SENSORHAL := true
#NANOHUB_SENSORHAL_SENSORLIST := $(LOCAL_PATH)/sensorhal/sensorlist.cpp
#NANOHUB_SENSORHAL_EXYNOS_CONTEXTHUB := true
#NANOHUB_SENSORHAL_NAME_OVERRIDE := sensors.$(TARGET_BOARD_PLATFORM)

#PRODUCT_PACKAGES += \
    $(NANOHUB_SENSORHAL_NAME_OVERRIDE) \
    activity_recognition.$(TARGET_BOARD_PLATFORM) \
    context_hub.default \
    android.hardware.sensors@1.0-impl \
    android.hardware.sensors@1.0-service

# sensor utilities
#PRODUCT_PACKAGES += \
    nanoapp_cmd

# sensor utilities (only for userdebug and eng builds)
ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
    nanotool \
    sensortest
endif
endif

ifeq ($(BOARD_USES_EXYNOS_SENSORS_DUMMY), true)
# Sensor HAL
PRODUCT_PACKAGES += \
	android.hardware.sensors@1.0-impl \
	android.hardware.sensors@1.0-service \
	sensors.$(TARGET_SOC)
endif

# USB HAL
PRODUCT_PACKAGES += \
	android.hardware.usb@1.1 \
	android.hardware.usb@1.1-service

# Lights HAL
PRODUCT_PACKAGES += \
	lights.default \
	android.hardware.light@2.0-impl \
	android.hardware.light@2.0-service

# Fastboot HAL
PRODUCT_PACKAGES += \
       fastbootd \
       android.hardware.fastboot@1.0\
       android.hardware.fastboot@1.0-impl-mock.$(TARGET_SOC) \

# Power HAL
PRODUCT_PACKAGES += \
	android.hardware.power@1.0-impl \
	android.hardware.power@1.0-service \
	power.$(TARGET_SOC)

# configStore HAL
PRODUCT_PACKAGES += \
	android.hardware.configstore@1.0-service \
	android.hardware.configstore@1.0-impl

# OFI HAL
PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.ofi@2.1-service \
	libofi_dal \
	libofi_kernels_cpu \
	libofi_klm_vendor \
	libofi_plugin_vendor \
	libofi_rt_framework \
	libofi_rt_framework_user_vendor \
	libofi_service_interface_vendor \
	libofi_seva_vendor \
	libinference_engine_vendor \
	libofi_plugin_vendor

PRODUCT_SOONG_NAMESPACES += \
	vendor/samsung_slsi/exynos/ofi/2.1 \
	vendor/samsung_slsi/hardware/ofi/2.1/default


PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/firmware/dsp/dsp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp.bin \
	device/samsung/erdv9630/firmware/dsp/dsp_iac_dm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_dm.bin \
	device/samsung/erdv9630/firmware/dsp/dsp_iac_pm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_pm.bin \
	device/samsung/erdv9630/firmware/dsp/dsp_ivp_dm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_dm.bin \
	device/samsung/erdv9630/firmware/dsp/dsp_ivp_pm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_pm.bin \
	device/samsung/erdv9630/firmware/dsp/dsp_master.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_master.bin \
	device/samsung/erdv9630/firmware/dsp/dsp_reloc_rules.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_reloc_rules.bin \
	device/samsung/erdv9630/firmware/dsp/libivp.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libivp.elf \
	device/samsung/erdv9630/firmware/dsp/liblog.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/liblog.elf

# exynosdisplayfeature
PRODUCT_PACKAGES += \
  vendor.samsung_slsi.hardware.exynosdisplayfeature@1.0 \
  vendor.samsung_slsi.hardware.exynosdisplayfeature@1.0-impl \
  vendor.samsung_slsi.hardware.exynosdisplayfeature@1.0-service

# Thermal HAL
PRODUCT_PACKAGES += \
	android.hardware.thermal@2.0-impl \
	android.hardware.thermal@2.0-service.exynos

#Health 1.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.0-service

#
# Audio HALs
#

# Audio Configurations
USE_LEGACY_LOCAL_AUDIO_HAL := false
USE_XML_AUDIO_POLICY_CONF := 1


ifneq ($(filter full_erdv9630_qr full_erdv9630_qrs, $(TARGET_PRODUCT)),)
# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
	android.hardware.audio@2.0-service \
	android.hardware.audio@5.0-impl \
	android.hardware.audio.effect@5.0-impl \
	android.hardware.soundtrigger@2.0-impl
else
# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
	android.hardware.audio@2.0-service \
	android.hardware.audio@4.0-impl \
	android.hardware.audio.effect@4.0-impl \
	android.hardware.soundtrigger@2.0-impl
endif

# AudioHAL libraries
PRODUCT_PACKAGES += \
	audio.primary.$(TARGET_SOC) \
	audio.usb.default \
	audio.r_submix.default

# AudioHAL Configurations
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/audio/config/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/a2dp_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
	device/samsung/erdv9630/audio/config/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
	frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \
	device/samsung/erdv9630/audio/config/audio_board_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_board_info.xml

ifeq ($(BOARD_USE_USB_OFFLOAD), true)
PRODUCT_COPY_FILES += \
        device/samsung/erdv9630/audio/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml
else
PRODUCT_COPY_FILES += \
        frameworks/av/services/audiopolicy/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml
endif

# Mixer Path Configuration for AudioHAL
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/audio/config/mixer_paths.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths.xml

# AudioEffectHAL Configuration & Library
ifeq ($(BOARD_USE_OFFLOAD_EFFECT),true)
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/audio/config/audio_effects_withoffload.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml
PRODUCT_PACKAGES += \
	libbundlewrapper_offload \
	libreverbwrapper_offload
else
PRODUCT_COPY_FILES += \
	frameworks/av/media/libeffects/data/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml
endif

# MIDI support native XML
PRODUCT_COPY_FILES += \
        frameworks/native/data/etc/android.software.midi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.midi.xml

# Enable AAudio MMAP/NOIRQ data path.
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_policy=1
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_exclusive_policy=1
PRODUCT_PROPERTY_OVERRIDES += aaudio.hw_burst_min_usec=2000

# Calliope firmware overwrite
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/firmware/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/erdv9630/firmware/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/erdv9630/firmware/calliope_dram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram_2.bin \
	device/samsung/erdv9630/firmware/calliope_sram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram_2.bin \
	device/samsung/erdv9630/firmware/calliope2.dt:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope2.dt \
	device/samsung/erdv9630/firmware/abox_tplg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.bin \
	device/samsung/erdv9630/firmware/abox_tplg.conf:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.conf \
	device/samsung/erdv9630/firmware/param_aprxse_v2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_aprxse.bin \
	device/samsung/erdv9630/firmware/param_aptxse_v2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_aptxse.bin \
	device/samsung/erdv9630/firmware/param_cprxse_v2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_cprxse.bin \
	device/samsung/erdv9630/firmware/param_cptxse_v2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_cptxse.bin \
	device/samsung/erdv9630/firmware/rxse_v2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/rxse.bin \
	device/samsung/erdv9630/firmware/txse_v2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse.bin \
	device/samsung/erdv9630/firmware/AP_AUDIO_SLSI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/AP_AUDIO_SLSI.bin \
	device/samsung/erdv9630/firmware/dsm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsm.bin \
	device/samsung/erdv9630/firmware/Tfa9874.cnt:$(TARGET_COPY_OUT_VENDOR)/firmware/Tfa9874.cnt

# SoundTriggerHAL library
ifeq ($(BOARD_USE_SOUNDTRIGGER_HAL), true)
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/firmware/vts.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/vts.bin
PRODUCT_PACKAGES += \
        sound_trigger.primary.$(TARGET_SOC)
endif

# AudioEffectHAL library
ifeq ($(BOARD_USE_OFFLOAD_AUDIO), true)
ifeq ($(BOARD_USE_OFFLOAD_EFFECT),true)
PRODUCT_PACKAGES += \
	libexynospostprocbundle
endif
endif

# A-Box Service Daemon
PRODUCT_PACKAGES += main_abox
# Audio Dump Daemon
ifeq ($(BOARD_USES_AUDIO_LOGGING_SERVICE), true)
PRODUCT_PACKAGES += \
	audiodumpd \
	AudioLogging
endif


# control_privapp_permissions
PRODUCT_PROPERTY_OVERRIDES += ro.control_privapp_permissions=enforce

# TinyTools for Audio
ifneq ($(filter eng userdebug,$(TARGET_BUILD_VARIANT)),)
PRODUCT_PACKAGES += \
    tinyplay \
    tinycap \
    tinymix \
    tinypcminfo \
    cplay
endif

# Libs
PRODUCT_PACKAGES += \
	com.android.future.usb.accessory

# for now include gralloc here. should come from hardware/samsung_slsi/exynos5
PRODUCT_PACKAGES += \
    android.hardware.graphics.mapper@2.0-impl-2.1 \
    android.hardware.graphics.allocator@2.0-service \
    android.hardware.graphics.allocator@2.0-impl \
    gralloc.$(TARGET_SOC)

PRODUCT_PACKAGES += \
    memtrack.$(TARGET_BOOTLOADER_BOARD_NAME)\
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    ionps \
    libion

PRODUCT_PACKAGES += \
    libhwjpeg

# Video Editor
#PRODUCT_PACKAGES += \
	VideoEditorGoogle

# WideVine modules
PRODUCT_PACKAGES += \
	android.hardware.drm@1.0-impl \
	android.hardware.drm@1.0-service \
	android.hardware.drm@1.4-service.clearkey \
	android.hardware.drm@1.4-service.widevine

# SecureDRM modules
PRODUCT_PACKAGES += \
	tlwvdrm \
	tlsecdrm \
	liboemcrypto_modular

# CryptoManager
PRODUCT_PACKAGES += \
        tlcmdrv

# CryptoManager test app for eng build
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
	tlcmtest \
	cm_test
endif

#Mobicore
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/kinibi500

# MobiCore setup
PRODUCT_PACKAGES += \
	libMcClient \
	libMcRegistry \
	libgdmcprov \
	mcDriverDaemon \
	vendor.trustonic.tee@1.1-service \
	RootPA \
	TeeService \
	libTeeClient \
	libteeservice_client.trustonic \
	tee_tlcm \
	tee_whitelist \
	public.libraries-trustonic.txt

# SPI driver
PRODUCT_PACKAGES += \
	 tlspidrv

# Camera HAL
#PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-impl \
    android.hardware.camera.provider@2.4-service \
    camera.$(TARGET_SOC)

# Copy FIMC_IS DDK Libraries
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/camera/is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_lib.bin \
    device/samsung/erdv9630/firmware/camera/is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_rta.bin \
    device/samsung/erdv9630/firmware/camera/setfile_gm1sp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_gm1sp.bin \
    device/samsung/erdv9630/firmware/camera/setfile_5e9.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_5e9.bin \
    device/samsung/erdv9630/firmware/camera/setfile_4h5yc.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_4h5yc.bin
#    device/samsung/erdv9630/firmware/camera/setfile_2p7sq.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2p7sq.bin \
    device/samsung/erdv9630/firmware/camera/setfile_2t7sx.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2t7sx.bin \
    device/samsung/erdv9630/firmware/camera/setfile_6b2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_6b2.bin \
    device/samsung/erdv9630/firmware/camera/setfile_2x5sp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2x5sp.bin\

# Copy OIS Libraries
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/camera/bu24218_cal_data_default.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bu24218_cal_data_default.bin \
    device/samsung/erdv9630/firmware/camera/bu24218_Rev1.5_S_data1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bu24218_Rev1.5_S_data1.bin \
    device/samsung/erdv9630/firmware/camera/bu24218_Rev1.5_S_data2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bu24218_Rev1.5_S_data2.bin

# Copy VIPx Firmware
#PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/camera/vipx/CC_DRAM_CODE_FLASH_HIFI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DRAM_CODE_FLASH_HIFI.bin \
    device/samsung/erdv9630/firmware/camera/vipx/CC_DTCM_CODE_FLASH_HIFI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DTCM_CODE_FLASH_HIFI.bin \
    device/samsung/erdv9630/firmware/camera/vipx/CC_ITCM_CODE_FLASH_HIFI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_ITCM_CODE_FLASH_HIFI.bin

# Copy Camera HFD Setfiles
#PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/camera/libhfd/default_configuration.hfd.cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.hfd.cfg.json \
    device/samsung/erdv9630/firmware/camera/libhfd/pp_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/pp_cfg.json \
    device/samsung/erdv9630/firmware/camera/libhfd/tracker_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/tracker_cfg.json \
    device/samsung/erdv9630/firmware/camera/libhfd/WithLightFixNoBN.SDNNmodel:$(TARGET_COPY_OUT_VENDOR)/firmware/WithLightFixNoBN.SDNNmodel

PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \
	frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.flash-autofocus.xml \
	frameworks/native/data/etc/android.hardware.camera.full.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.full.xml \
	frameworks/native/data/etc/android.hardware.camera.raw.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.raw.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.passpoint.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.passpoint.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/native/data/etc/android.hardware.audio.pro.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.pro.xml \

# WLAN configuration
# device specific wpa_supplicant.conf
PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/wpa_supplicant.conf:vendor/etc/wifi/wpa_supplicant.conf \
        device/samsung/erdv9630/wifi/p2p_supplicant.conf:vendor/etc/wifi/p2p_supplicant.conf

# Bluetooth libbt namespace
#PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/libbt

# Bluetooth libbt namespace
#PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/libbt

# Bluetooth configuration
#PRODUCT_COPY_FILES += \
       frameworks/native/data/etc/android.hardware.bluetooth.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth.xml \
       hardware/samsung_slsi/libbt/conf/bt_did.conf:$(TARGET_COPY_OUT_VENDOR)/etc/bluetooth/bt_did.conf \
       frameworks/native/data/etc/android.hardware.bluetooth_le.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth_le.xml


PRODUCT_PROPERTY_OVERRIDES += \
        wifi.interface=wlan0

# Packages needed for WLAN
# Note android.hardware.wifi@1.0-service is used by HAL interface 1.2
PRODUCT_PACKAGES += \
    android.hardware.wifi@1.0-service \
    android.hardware.wifi.supplicant@1.1-service \
    android.hardware.wifi.offload@1.0-service \
    dhcpcd.conf \
    hostapd \
    wificond \
    wifilog \
    wpa_supplicant.conf \
    wpa_supplicant \
    wpa_cli \
    hostapd_cli

PRODUCT_PACKAGES += \
    libwifi-hal \
    libwifi-system \
    libwifikeystorehal \
    android.hardware.wifi.supplicant@1.2 \
    android.hardware.wifi@1.3

# Bluetooth HAL
#PRODUCT_PACKAGES += \
    android.hardware.bluetooth@1.0-service \
    android.hardware.bluetooth@1.0-impl \
    libbt-vendor

#PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    ro.bt.bdaddr_path=/sys/module/scsc_bt/parameters/bluetooth_address_fallback

PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=196610 \
	ro.sf.lcd_density=480 \
	debug.slsi_platform=1 \
	debug.hwc.winupdate=1 \
	debug.sf.disable_backpressure=1

# adoptable storage: these properties are moved to system.prop
PRODUCT_PROPERTY_OVERRIDES += ro.crypto.volume.filenames_mode=aes-256-cts
#PRODUCT_PROPERTY_OVERRIDES += ro.crypto.allow_encrypt_override=true

# LMK
PRODUCT_PROPERTY_OVERRIDES += \
	ro.lmk.use_minfree_levels=true

# hw composer HAL
PRODUCT_PACKAGES += \
    hwcomposer.$(TARGET_BOOTLOADER_BOARD_NAME) \
    android.hardware.graphics.composer@2.2-impl \
    android.hardware.graphics.composer@2.2-service \
    vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service

PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.hwc.force_native_color_mode=1

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS5_DSS_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.exynos.dss=1
endif

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS_AFBC_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.vendor.ddk.set.afbc=1
endif

PRODUCT_CHARACTERISTICS := phone

PRODUCT_AAPT_CONFIG := normal hdpi xhdpi xxhdpi
PRODUCT_AAPT_PREF_CONFIG := xxhdpi

####################################
## VIDEO
####################################
# MFC firmware
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/mfc_fw_v14.10.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/mfc_fw.bin

PRODUCT_PACKAGES += \
    libstagefrighthw \
    libExynosOMX_Core \
    libExynosOMX_Resourcemanager \
    libOMX.Exynos.MPEG4.Decoder \
    libOMX.Exynos.AVC.Decoder \
    libOMX.Exynos.WMV.Decoder \
    libOMX.Exynos.VP8.Decoder \
    libOMX.Exynos.HEVC.Decoder \
    libOMX.Exynos.VP9.Decoder \
    libOMX.Exynos.MPEG4.Encoder \
    libOMX.Exynos.AVC.Encoder \
    libOMX.Exynos.VP8.Encoder \
    libOMX.Exynos.HEVC.Encoder \
    libOMX.Exynos.VP9.Encoder

# mediacodec.policy
PRODUCT_COPY_FILES += \
 	device/samsung/erdv9630/seccomp_policy/mediacodec-seccomp.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/mediacodec.policy

# OpenMAX IL configuration files
PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_audio.xml \
	frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_telephony.xml \
	frameworks/av/media/libstagefright/data/media_codecs_google_video.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_video.xml \
	device/samsung/erdv9630/media_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_V1_0.xml \
	device/samsung/erdv9630/media_codecs_performance.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance.xml \
	device/samsung/erdv9630/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml \
	device/samsung/erdv9630/media_codecs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs.xml

# AVIExtractor
PRODUCT_PACKAGES += \
	libaviextractor

ifeq ($(BOARD_USE_DEFAULT_SERVICE), true)
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/c2service

DEVICE_MANIFEST_FILE += \
	device/samsung/erdv9630/manifest_media_c2_default.xml

PRODUCT_PACKAGES += \
    samsung.hardware.media.c2@1.1-default-service
endif

# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

PRODUCT_TAGS += dalvik.gc.type-precise

# Exynos OpenVX framework
#PRODUCT_PACKAGES += \
		libexynosvision

ifeq ($(TARGET_USES_CL_KERNEL),true)
PRODUCT_PACKAGES += \
       libopenvx-opencl
endif

#GPS
#PRODUCT_PACKAGES += \
    android.hardware.gnss@1.0-impl \
    android.hardware.gnss@1.1-impl \
    android.hardware.gnss@2.0-impl \
    android.hardware.gnss@2.1-impl \
    vendor.samsung.hardware.gnss@1.0-impl \
    vendor.samsung.hardware.gnss@1.0-service

#PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.gnsslog.maxfilesize=256 \
    persist.vendor.gnsslog.status=0 \
    vendor.exynos.gnsslog=/data/vendor/gps/

#Gatekeeper
PRODUCT_PACKAGES += \
	android.hardware.gatekeeper@1.0-impl \
	android.hardware.gatekeeper@1.0-service \
	gatekeeper.$(TARGET_SOC)

# KeyManager/AES modules
#PRODUCT_PACKAGES += \
	tlkeyman

# Eden
PRODUCT_PACKAGES += \
       android.hardware.neuralnetworks@1.3-service.eden-drv \
       vendor.samsung_slsi.hardware.eden_runtime@1.0-impl \
       vendor.samsung_slsi.hardware.eden_runtime@1.0-service
PRODUCT_PROPERTY_OVERRIDES += \
       log.tag.EDEN=INFO \
	   ro.vendor.eden.basecore=6 \
       ro.vendor.eden.devices=CPU1_GPU1_NPU1 \
       ro.vendor.eden.npu.version.path=/sys/devices/platform/npu_exynos/version

PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/eden/eden_kernel_64.bin:$(TARGET_COPY_OUT_VENDOR)/etc/eden/gpu/eden_kernel_64.bin

# Keymaster
PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0-impl \
	android.hardware.keymaster@4.0_tee-service \
	tlkeymasterM \
	keymaster_drv

PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/TlcTeeKeymaster4

ifneq ($(filter eng userdebug,$(TARGET_BUILD_VARIANT)),)
PRODUCT_PACKAGES += \
	km_keybox_attestation
endif

# ArmNN driver
#PRODUCT_PACKAGES += \
  android.hardware.neuralnetworks@1.1-service-armnn

PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/platform/13600000.ufs/by-name/frp

# RenderScript HAL
PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

# FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml

# vulkan version information
	PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.compute-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.compute.xml \
	frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml \
	frameworks/native/data/etc/android.software.vulkan.deqp.level-2019-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml

# AVB2.0, to tell PackageManager that the system supports Verified Boot(PackageManager.FEATURE_VERIFIED_BOOT)
ifeq ($(BOARD_AVB_ENABLE), true)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.verified_boot.xml:system/etc/permissions/android.software.verified_boot.xml
endif

#VNDK
PRODUCT_PACKAGES += \
	vndk-libs

PRODUCT_ENFORCE_RRO_TARGETS := \
	framework-res

#VENDOR
PRODUCT_PACKAGES += \
	libGLES_mali \
	libGLES_mali32 \
	libOpenCL \
	libOpenCL32 \
	whitelist

PRODUCT_PACKAGES += \
	mfc_fw.bin \
	#calliope_sram.bin \
	calliope_dram.bin

PRODUCT_PACKAGES += \
	exynos-thermald

# Epic daemon
PRODUCT_COPY_FILES += \
        device/samsung/erdv9630/power/epic.json:$(TARGET_COPY_OUT_VENDOR)/etc/epic.json

PRODUCT_PACKAGES += epic libepic_helper

# Copy Camera NFD Setfiles
PRODUCT_COPY_FILES += \
    device/samsung/erdv9630/firmware/camera/libvpl/default_configuration.flm.cfg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.flm.cfg.bin \
    device/samsung/erdv9630/firmware/camera/libvpl/NPU_NFD_P4.6.tflite:$(TARGET_COPY_OUT_VENDOR)/firmware/NPU_NFD_P4.6.tflite

# Epic HIDL
SOONG_CONFIG_NAMESPACES += epic
SOONG_CONFIG_epic := vendor_hint
SOONG_CONFIG_epic_vendor_hint := true

PRODUCT_PACKAGES += \
        vendor.samsung_slsi.hardware.epic@1.0-impl \
        vendor.samsung_slsi.hardware.epic@1.0-service

#$(call inherit-product-if-exists, vendor/samsung_slsi/telephony/common/device-vendor.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)
#$(call inherit-product, hardware/samsung_slsi/exynos5/exynos5.mk)
$(call inherit-product-if-exists, hardware/samsung_slsi/exynos9630/exynos9630.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/common/exynos-vendor.mk)
#$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/eden/eden.mk)
#$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/camera/hal3/camera.mk)
#$(call inherit-product, device/samsung/erdv9630/gnss_binaries/gnss_binaries.mk)

ifneq ($(filter full_erdv9630_qr full_erdv9630_qrs, $(TARGET_PRODUCT)),)
# Installs gsi keys into ramdisk, to boot a GSI with verified boot.
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)
endif

## MAKE mx file for WLBT ##
ifneq ($(findstring mx, $(wildcard vendor/samsung_slsi/mx140/firmware/mx)),)
PRODUCT_COPY_FILES += vendor/samsung_slsi/mx140/firmware/mx:vendor/etc/wifi/mx
endif

## WLBT Logging ##
#PRODUCT_PROPERTY_OVERRIDES += \
        persist.vendor.wlbtlog.maxfilesize=50 \
        persist.vendor.wlbtlog.maxfiles=5

#$(call inherit-product, device/samsung/erdv9630/scsc_wlbt.mk)
#$(call inherit-product, vendor/samsung_slsi/scsc_tools/wlbt/device-vendor.mk)

$(warning samsung facotry: $(TARGET_USE_FACTORY_IMAGES))
$(warning samsung factory in normal: $(TARGET_SUPPORT_MPTOOL_IN_NORMAL_MODE))
$(warning current dir : $(dir $(TARGET_BOARD_INFO_FILE)))
## IMSService ##
# IMSService build for both types (source build, prebulit apk/so build).
# Please make sure that only one of both ShannonIms(apk git) shannon-ims(src git) is present in the repo.
# This will be called only if IMSService is building with prebuilt binary for stable branches.
#$(call inherit-product-if-exists, packages/apps/ShannonIms/device-vendor.mk)
#$(call inherit-product-if-exists, packages/apps/ShannonIms/device-vendor-testauto.mk)
# This will be called only if IMSService is building with source code for dev branches.
#$(call inherit-product-if-exists, vendor/samsung_slsi/ims/shannon-ims/device-vendor.mk)
#$(call inherit-product-if-exists, vendor/samsung_slsi/ims/shannon-iwlan/device-vendor.mk)
#$(call inherit-product-if-exists, vendor/samsung_slsi/ims/PacketRouter/device-vendor.mk)
#$(call inherit-product-if-exists, external/strongswan/device-vendor.mk)


#mptool
#include $(dir $(TARGET_BOARD_INFO_FILE))mptool/device.mk

ifneq ($(filter eng userdebug,$(TARGET_BUILD_VARIANT)),)
PRODUCT_PACKAGES += erdv9630_dssd \
                    erdv9630_dssd_qd \
                    erdv9630_dssd_diary
endif

# NFC Feature
$(call inherit-product-if-exists, device/samsung/erdv9630/nfc/device-nfc.mk)

PRODUCT_COPY_FILES += \
	device/samsung/erdv9630/exynos-thermal/exynos-thermal.conf:$(TARGET_COPY_OUT_VENDOR)/exynos-thermal.conf \
	device/samsung/erdv9630/exynos-thermal/exynos-thermal.env:$(TARGET_COPY_OUT_VENDOR)/exynos-thermal.env

# control oem unlock property
PRODUCT_PROPERTY_OVERRIDES += ro.oem_unlock_supported=1

# GMS Applications
ifeq ($(WITH_GMS),true)
GMS_ENABLE_OPTIONAL_MODULES := true
USE_GMS_STANDARD_CONFIG := true
$(call inherit-product-if-exists, vendor/partner_gms/products/gms.mk)
endif

#supplicant configs
ifneq ($(BOARD_WPA_SUPPLICANT_DRIVER),)
CONFIG_DRIVER_$(BOARD_WPA_SUPPLICANT_DRIVER) := y
endif
WPA_SUPPL_DIR = external/wpa_supplicant_8
include $(WPA_SUPPL_DIR)/wpa_supplicant/wpa_supplicant_conf.mk
include $(WPA_SUPPL_DIR)/wpa_supplicant/android.config

# hw composer property : Properties related to hwc will be defined in hwcomposer_property.mk
$(call inherit-product-if-exists, hardware/samsung_slsi/graphics/base/hwcomposer_property.mk)
