/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __EXYNOS_AUDIOPROXY_MIXER_H__
#define __EXYNOS_AUDIOPROXY_MIXER_H__

#include <audio_route/audio_route.h>

/* Mixer Card Definition */
#define MIXER_CARD0     0


#define MAX_PATH_NAME_LEN 50
#define MAX_GAIN_PATH_NAME_LEN 55 //"gain-" + path_name size

/* Mixer Controls for ERAP Handling */
#define MAX_MIXER_NAME_LEN 50

// Mixer Control for set USB Mode
#define ABOX_USBMODE_CONTROL_NAME "ABOX ERAP info USB On"

// USB Clock Source inforamtion mixer control
#define MIXER_CTL_ABOX_USB_CLOCKSOURCE   "ABOX PCM ext USB SCDS"

// USB Device Async inforamtion mixer control
#define MIXER_CTL_ABOX_USB_DEV_ASYNCINFO "ABOX PCM ext USB OUT ASync"

/**
 ** Sampling Rate, channels & format Modifier Configuration for USB playback
 **/
// SIFS0 config controls
#define ABOX_SAMPLE_RATE_MIXER_NAME     "ABOX SIFS0 Rate"
#define ABOX_CHANNELS_MIXER_NAME        "ABOX SIFS0 Channel"
#define ABOX_BIT_WIDTH_MIXER_NAME       "ABOX SIFS0 Width"

// RDMA4 config controls
#define ABOX_RDMA4_SAMPLE_RATE          "ABOX RDMA4 Rate"
#define ABOX_RDMA4_PERIOD_SIZE          "ABOX RDMA4 Period"

// WDMA0 config controls
#define ABOX_CHANNELS_WDMA0_NAME        "ABOX WDMA0 Channel"

// WDMA3 config controls
#define ABOX_SAMPLE_RATE_WDMA3_NAME     "ABOX WDMA3 Rate"
#define ABOX_CHANNELS_WDMA3_NAME        "ABOX WDMA3 Channel"
#define ABOX_BIT_WIDTH_WDMA3_NAME       "ABOX WDMA3 Width"
#define ABOX_PERIOD_SIZE_WDMA3_NAME     "ABOX WDMA3 Period"
#define ABOX_SPUM_ASRC_WDMA3            "ABOX SPUM ASRC3"

// Mixer Control for set MUTE Control
#define ABOX_MUTE_CONTROL_NAME "ABOX ERAP info Mute Primary"
#define ABOX_MUTE_CNT_FOR_PATH_CHANGE 15

// Mixer Control for set A-Box Early WakeUp Control
#define ABOX_TICKLE_CONTROL_NAME "ABOX Tickle"
#define ABOX_TICKLE_ON      1

typedef enum {
    USBMODE        = 0,
    MUTE_CONTROL,
    TICKLE_CONTROL,
} erap_trigger;

// Mixer Control for set Android Audio Mode
#define ABOX_AUDIOMODE_CONTROL_NAME "ABOX Audio Mode"


// Compress Offload Volume
#define OFFLOAD_VOLUME_CONTROL_NAME "ABOX ComprTx0 Volume"
#define COMPRESS_PLAYBACK_VOLUME_MAX   8192

// Compress Offload Upscaling
#define OFFLOAD_UPSCALE_CONTROL_NAME "ABOX ComprTx0 Format"

// AP Call volume
#define AP_CALL_VOLUME_NAME "ABOX Speech Volume"

typedef enum {
    UPSCALE_NONE        = 0,
    UPSCALE_48K_16B,
    UPSCALE_48K_24B,
    UPSCALE_192K_24B,
    UPSCALE_384K_24B,
} upscale_factor;

// Mixer for Call Path Parameter
#define ABOX_CALL_PATH_PARAM "ABOX Call Path Param"
#define ABOX_RXSE_VOL        "ABOX RXSE Volume"
#define ABOX_TXSE_MUTE       "ABOX TXSE Mute"
#define ABOX_RXSE_MUTE       "ABOX RXSE Mute"
#define ABOX_RX_MUTE         "ABOX RX Mute"
#define ABOX_INCALL_MUSIC_VOL  "ABOX Incall Music Volume"
#define ABOX_INCALL_MUSIC_MUTE "ABOX Incall Music Mute"
#define RXSE_VOL_MAX         5  // could be changed by Customer

/* Call parameter - CALL_TYPE from RIL */
typedef enum {
    GSM = 1,
    CDMA = 2,
    IMS = 4,
    ETC = 8,
} call_type_fromRil;

/* Call parameter - CHANNEL */
enum {
    MONO = 1,
    STEREO,
};
/* Call parameter - The number of Mic */
enum {
    MIC0 = 0,
    MIC1,
    MIC2,
};

/* Call parameter - Device */
enum {
    HANDSET = 0,
    HEADPHONE,
    HEADSET,
    SPEAKER,
    BT_SCO,
    USB_HEADSET,
};

/* Mixer control for UAIF configuration */
// UAIF0 config controls
#define MIXER_CTL_ABOX_UAIF0_SWITCH      "ABOX UAIF0 Switch"
#define MIXER_CTL_ABOX_UAIF0_SAMPLERATE  "ABOX UAIF0 Rate"
#define MIXER_CTL_ABOX_UAIF0_WIDTH       "ABOX UAIF0 Width"
#define MIXER_CTL_ABOX_UAIF0_CHANNEL     "ABOX UAIF0 Channel"

// SIFS0 config controls
#define MIXER_CTL_ABOX_SIFS0_SWITCH      "ABOX SIFS0 OUT Switch"
#define MIXER_CTL_ABOX_SIFS0_SAMPLERATE  "ABOX SIFS0 Rate"
#define MIXER_CTL_ABOX_SIFS0_WIDTH       "ABOX SIFS0 Width"
#define MIXER_CTL_ABOX_SIFS0_CHANNEL     "ABOX SIFS0 Channel"

#define MIXER_CTL_ABOX_SIFS2_SWITCH      "ABOX SIFS2 OUT Switch"
#define MIXER_CTL_ABOX_SIFS3_SWITCH      "ABOX SIFS3 OUT Switch"

#define MIXER_ON                         1
#define MIXER_OFF                        0

#endif  // __EXYNOS_AUDIOPROXY_MIXER_H__
